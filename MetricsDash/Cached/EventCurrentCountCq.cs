﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsDash.Cached
{
    public class EventCurrentCountCq : LiveMetricCountBase , ICachedItemQuery
    {
        public EventCurrentCountCq(IMetricsRepo<StatConfig> metricRepo) : base(metricRepo)
        {
            Connectors = new List<int>() { 2, 3, 4, 5 };
            Type = "EventCurrentCount";

            _Metrics = new List<string>()
            {
                "EVENTS_ALL",
                "EVENTS_COMM_ACCESS",
                "EVENTS_COMM",
                "EVENTS_GRANTED",
                "EVENTS_DENIED",
                "EVENTS_FORCED",
                "EVENTS_HELD",
                "EVENTS_GLASS",
                "EVENTS_DURESS",
                "EVENTS_FIRE",
                "WS_API_REST_BADGE",
                "WS_API_SOAP_BADGE"
            };

        }
    }
}
