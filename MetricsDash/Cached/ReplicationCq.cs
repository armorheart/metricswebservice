﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Cached
{
    public class ReplicationCqModel
    {
        public string ReplTask { get; set; }
        public string CurrentStatus { get; set; }
        public DateTime CompletedDate { get; set; }
        public int Processed { get; set; }
        public DateTime LastSuccessDate { get; set; }
        public int MinsSinceLast { get; set; }

    }

    public class ReplicationCq : ICachedItemQuery
    {
        public List<int> Connectors { get; private set; }
        public string Type { get; private set; }
        public int Timeout { get; set; }

        private List<ReplicationCqModel>  _Data = new List<ReplicationCqModel>();

        public ReplicationCq()
        {
            Connectors = new List<int>() { 2, 3, 4, 5 };
            Type = "Replication";
            Timeout = 150;
        }

        public void RunQuery(string connStr, string regionName)
        {
            _Data.Clear();
            using (var conn = new SqlConnection(connStr))
            {
                using (var cmd = new SqlCommand(Sql, conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = Timeout;
                    var reader = cmd.ExecuteReader();
                    while (reader.HasRows && reader.Read())
                    {
                        var line = new ReplicationCqModel();
                        line.ReplTask = reader["Replication Type"].ToString();
                        line.CurrentStatus = reader["Current Status"].ToString();
                        line.CompletedDate = (DateTime)reader["Completed On"];
                        //this can be null
                        int  processed = 0;
                        Int32.TryParse(reader["To Replicate Count"].ToString(), out processed);
                        line.Processed = processed;
                        
                        line.LastSuccessDate = (DateTime)reader["Last Successful"];
                        line.MinsSinceLast = (int)reader["Minutes Ago"];

                        _Data.Add(line);
                    }
                }
            }
        }

        public object GetData()
        {
            return _Data;
        }




        private const string Sql =  @" SELECT TOP (100) PERCENT 
	CASE REPL_TASKID /* Replication Type*/ 
	  WHEN 1 THEN 'Cardholder Upload' 
	  WHEN 2 THEN 'Cardholder Download' 
	  WHEN 4 THEN 'System Table Download' 
	  WHEN 8 THEN 'Events Upload'
	  WHEN 32 THEN 'Last Location' 
	  ELSE 'Unknown Replication' END AS [Replication Type], 
	CASE AL.RUNSTATUS /* Current Status*/ 
	  WHEN 0 THEN 'Started' 
	  WHEN 1 THEN 'In Progress' 
	  WHEN 2 THEN 'Completed' 
	  ELSE 'Unknown Status' END AS [Current Status], 
	  
	AL.RUNTIME AS [Completed On], 
	AL.MESSAGE AS [Status Message], 

	CASE REPL_TASKID /* Replication Count*/ 
	  /* Cardholder Upload*/
	  WHEN 1 THEN
		 CASE AL.RUNSTATUS 
			  /* Started*/
			  WHEN 0 THEN
				  (SELECT     COUNT(*)
					FROM          BDG2TRN
					WHERE      T_TIME < AL.RUNTIME AND TRANSTATUS = 0) 
			  /* In Progress*/
			  WHEN 1 THEN
				  (SELECT     COUNT(*)
					FROM          BDG2TRN
					WHERE      T_TIME <
						   (SELECT     TOP 1 RUNTIME
							 FROM          ACTION_LOG AL_INNER
							 WHERE      AL_INNER.ACTIONID = AL.ACTIONID AND AL_INNER.RUNSTATUS = 0 AND AL_INNER.RUNTIME < AL.RUNTIME
							 ORDER BY AL_INNER.RUNTIME DESC) AND TRANSTATUS = 0) 
			  /* Completed*/
			  WHEN 2 THEN
				  (SELECT     COUNT(*)
					FROM          BDG2TRN
					WHERE      TRANSTATUS = 0) END 
	  /* Cardholder Download*/
	  WHEN 2 THEN 
		  CASE AL.RUNSTATUS 
			  /* Started*/
			  WHEN 0 THEN
				  (SELECT     COUNT(*)
					FROM          [secsql01a].[AccessControl_RCDN_SEC].dbo.BDG2TRN
					WHERE      T_TIME < AL.RUNTIME AND TRANSTATUS = 0 AND LNL_DBID =
											   (SELECT     LNLVALUE
												 FROM          LNLCONFIG
												 WHERE      LNLCONFIGID = 1)) 
			 /* In Progress*/                                
			 WHEN 1 THEN
				  (SELECT     COUNT(*)
					FROM          [secsql01a].[AccessControl_RCDN_SEC].dbo.BDG2TRN
					WHERE      T_TIME <
											   (SELECT     TOP 1 RUNTIME
												 FROM          ACTION_LOG AL_INNER
												 WHERE      AL_INNER.ACTIONID = AL.ACTIONID AND AL_INNER.RUNSTATUS = 0 AND AL_INNER.RUNTIME < AL.RUNTIME
												 ORDER BY AL_INNER.RUNTIME DESC) AND TRANSTATUS = 0 AND LNL_DBID =
											   (SELECT     LNLVALUE
												 FROM          LNLCONFIG
												 WHERE      LNLCONFIGID = 1)) 
			/* Completed*/
			WHEN 2 THEN
				  (SELECT     COUNT(*)
					FROM          [secsql01a].[AccessControl_RCDN_SEC].dbo.BDG2TRN
					WHERE      TRANSTATUS = 0 AND LNL_DBID =
											   (SELECT     LNLVALUE
												 FROM          LNLCONFIG
												 WHERE      LNLCONFIGID = 1)) END 
	   /* System Table Download*/                         
	   WHEN 4 THEN
		  (SELECT     COUNT(NAME) 
			FROM          DDfld
			WHERE      FLDTYPE = 2) 
	  /* Events Upload*/
	  WHEN 8 THEN 
	  
		  CASE AL.RUNSTATUS 
			  /* Started*/
			  WHEN 0 THEN
				  (SELECT     COUNT(*)
					FROM          EVENTS_REPL
					WHERE      EVENT_TIME_UTC < AL.RUNTIME) 
					
			  /* InProgress*/      
			  WHEN 1 THEN
				  (SELECT     COUNT(*)
					FROM          EVENTS_REPL
					WHERE      EVENT_TIME_UTC <
											   (SELECT     TOP 1 RUNTIME
												 FROM          ACTION_LOG AL_INNER
												 WHERE      AL_INNER.ACTIONID = AL.ACTIONID AND AL_INNER.RUNSTATUS = 0 AND AL_INNER.RUNTIME < AL.RUNTIME
												 ORDER BY AL_INNER.RUNTIME DESC)) 
			  /* Completed*/                               
			  WHEN 2 THEN
				  (SELECT     COUNT(*)
					FROM          EVENTS_REPL) END 
					
	  /* Last Location */
	  WHEN 32 THEN 
	  
		  CASE AL.RUNSTATUS 
			  /* Started*/   
			  WHEN 0 THEN
				  (SELECT     COUNT(*)
					FROM          LASTLOCATION_REPL
					WHERE      EVENT_TIME_UTC < AL.RUNTIME) 
			  /* In Progress*/       
			  WHEN 1 THEN
				  (SELECT     COUNT(*)
					FROM          LASTLOCATION_REPL
					WHERE      EVENT_TIME_UTC <
											   (SELECT     TOP 1 RUNTIME
												 FROM          ACTION_LOG AL_INNER
												 WHERE      AL_INNER.ACTIONID = AL.ACTIONID AND AL_INNER.RUNSTATUS = 0 AND AL_INNER.RUNTIME < AL.RUNTIME
												 ORDER BY AL_INNER.RUNTIME DESC)) 
			  /* Completed*/                                 
			  WHEN 2 THEN
				  (SELECT     COUNT(*)
					FROM          LASTLOCATION_REPL) END END AS [To Replicate Count], 
	        
	WT.DisplayName AS 'Time Zone', 
	  
  (SELECT     MAX(RUNTIME) AS Expr1
    FROM          dbo.ACTION_LOG
    WHERE      (ACTIONID = AR.ACTIONID)) AS [Last Successful], 
DATEDIFF(MINUTE, (SELECT     MAX(RUNTIME) AS Expr1
    FROM          dbo.ACTION_LOG AS ACTION_LOG_1
    WHERE      (ACTIONID = AR.ACTIONID)), GETUTCDATE()) AS [Minutes Ago]
                            
FROM dbo.ACTION_LOG AS AL 
INNER JOIN dbo.ACTION_REPLICATOR AS AR ON AR.ACTIONID = AL.ACTIONID AND AR.REPL_TASKID <> 16 
INNER JOIN dbo.ACTION_SCHEDULE AS ASCH ON ASCH.ACTIONID = AR.ACTIONID 
INNER JOIN dbo.WORLDTZ AS WT ON WT.WORLDTZID = ASCH.WORLDTZID

WHERE     (AL.RUNTIME IN
                          (SELECT     MAX(RUNTIME) AS Expr1
                            FROM          dbo.ACTION_LOG AS AL2
                            WHERE      (ACTIONID IN
                                                       (SELECT     ACTIONID
                                                         FROM          dbo.ACTION_REPLICATOR
                                                         WHERE      (REPL_TASKID = AR.REPL_TASKID)))))
ORDER BY [Replication Type]";

       
    }
}
