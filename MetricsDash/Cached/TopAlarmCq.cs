﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MetricsDash.Cached
{
    public class TopAlarmCq : ICachedItemQuery
    {
        public List<int> Connectors { get; private set; }
        public string Type { get; private set; }
        public int Timeout { get; set; }

        private const string Sql = @"SELECT ALARM.ALDESCR,
              COUNT(EVENTS.SERIALNUM) as Alarms
            FROM   EVENTS 
              
                            LEFT OUTER JOIN EVTALMLINK ON 
                                    (EVENTS.EVENTTYPE = EVTALMLINK.EVTYPEID 
                                        AND EVENTS.EVENTID = EVTALMLINK.EVID
                                                AND EVENTS.MACHINE = EVTALMLINK.PANELID
                                                        AND EVENTS.DEVID = EVTALMLINK.DEVICEID
                                                            AND EVENTS.INPUTDEVID = EVTALMLINK.INPUTDEVID)
                                                
                            LEFT OUTER JOIN ALARM ON 
                                        (EVTALMLINK.ALID = ALARM.ALID)
                                                       
            WHERE  EVENTS.EVENT_TIME_UTC >= GETDATE() - 7        
                            AND ALARM.ALDESCR IS NOT NULL

            GROUP BY ALARM.ALDESCR
            ORDER BY Alarms desc ";

        private List<BasicCqModel> _Data = new List<BasicCqModel>();

        public TopAlarmCq()
        {
            Connectors = new List<int>() { 1, 2, 3, 4, 5 };
            Type = "TopAlarms";
            Timeout = 150;
        }

        public void RunQuery(string connStr, string regionName)
        {
            _Data.Clear();

            using (var conn = new SqlConnection(connStr))
            {
                using (var cmd = new SqlCommand(Sql, conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = Timeout;
                    var reader = cmd.ExecuteReader();
                    while (reader.HasRows && reader.Read())
                    {
                        _Data.Add(new BasicCqModel(reader["ALDESCR"].ToString(), reader["ALDESCR"].ToString(),
                            (int) reader["Alarms"]));
                    }

                    reader.Close();
                }
            }
        }

        public object GetData()
        {
            return _Data;
        }
    }
}