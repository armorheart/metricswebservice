﻿using System.Collections.Generic;

namespace MetricsDash.Cached
{
    public interface ICachedItemQuery
    {
        List<int> Connectors { get; }
        string Type { get; }
        int Timeout { get; set; }

        void RunQuery(string connStr, string regionName);
        object GetData();
    }
}