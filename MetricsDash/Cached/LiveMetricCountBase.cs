using System;
using System.Collections.Generic;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsDash.Cached
{
    public class LiveMetricCountBase 
    {
        private IMetricsRepo<StatConfig> _metricRepo;
        private List<BasicCqModel> _Data = new List<BasicCqModel>();

        /// <summary>
        /// Make this into a database table, or add a columm to StatConfig
        /// for queries without intervals
        /// </summary>
        protected List<string> _Metrics;

        public LiveMetricCountBase(IMetricsRepo<StatConfig> metricRepo )
        {
            _metricRepo = metricRepo;
            _Metrics = new List<string>();
            
        }

        public List<int> Connectors { get; protected set; }
        public string Type { get; protected set; }
        public int Timeout { get; set; }

        public void RunQuery(string connStr, string regionName)
        {
            _Data.Clear();

            foreach (var metric in _Metrics)
            {
                var config = _metricRepo.GetMetric(metric);

                try
                {
                    
                    var result = _metricRepo.RunMetricQuery(config, regionName, DateTime.Today);
                    _Data.Add(new BasicCqModel(config.ColumnName, config.DisplayName, result.Value));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    //Add placeholder data
                    _Data.Add(new BasicCqModel(config.ColumnName, config.DisplayName, 0));
                }
                
            }
        }

        public object GetData()
        {
            return _Data;
        }
    }
}