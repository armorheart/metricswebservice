﻿using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Cached
{
    public class BasicCqModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Count { get; set; }

        public BasicCqModel(string name, string desc, int count)
        {
            Name = name;
            Description = desc;
            Count = count;
        }

        public BasicCqModel() { }

    }
}
