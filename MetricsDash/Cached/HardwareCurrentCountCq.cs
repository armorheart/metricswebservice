﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsDash.Cached
{
    public class HardwareCurrentCountCq : LiveMetricCountBase, ICachedItemQuery
    {
        public HardwareCurrentCountCq(IMetricsRepo<StatConfig> metricRepo ) : base(metricRepo)
        {
            Connectors = new List<int>() { 2, 3, 4, 5 };
            Type = "HardwareCurrentCount";

            _Metrics = new List<string>()
            {
                "USERS_ACCOUNTS_ACTIVE",
                "HARDWARE_PANELS_LNL_ACTIVE",
                "HARDWARE_READERS",
                "HARDWARE_IPVIDEO_LNVR_ACTIVE",
                "HARDWARE_IPVIDEO_CAMERAS",
                "HARDWARE_IPVIDEO_CAMERAS_CISCO_ALL",
                "HARDWARE_IPVIDEO_CAMERAS_NOT_CISCO",
                "CONFIG_ACCESSLEVELS",
                "CONFIG_ALARMS_CUSTOM",
                "HARDWARE_PANELS_ACTIVE",
                "HARDWARE_ALARMPANEL_IN_ACTIVE",
                "HARDWARE_ALARMPANEL_OUT_ACTIVE",
                "HARDWARE_INPUT_ACTIVE",
                "HARDWARE_OUTPUT_ACTIVE"
            };

        }
    }
}
