﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsDash.Cached
{
    public class WorkforceBadgeCq : LiveMetricCountBase, ICachedItemQuery
    {
        public WorkforceBadgeCq(IMetricsRepo<StatConfig> metricRepo) : base(metricRepo)
        {
            Connectors = new List<int>() { 1 };
            Type = "WorkforceBadge";

            _Metrics = new List<string>()
            {
                "IDENTITY_CARDHOLDERS_ACTIVE",
                "IDENTITY_CARDHOLDER_EMP",
                "IDENTITY_CARDHOLDER_CONT",
                "IDENTITY_BADGES_EMP",
                "IDENTITY_BADGES_CONT",
                "IDENTITY_BADGES_SMART",
                "IDENTITY_BADGELINK",
                "ERT_TOTAL",
                "ERT_ACTIVE",
                "IDENTITY_BADGES_TEMP_ACTIVE",
                "IDENTITY_BADGES_SERVICE_ACTIVE",
                "IDENTITY_BADGES_CUSTOMER_ACTIVE",
                "IDENTITY_BADGES_SECURITY_ACTIVE",
                "IDENTITY_BADGES_CCC_ACTIVE"
            };
        }

        

        
    }
}
