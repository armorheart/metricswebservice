﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models.Details;

namespace MetricsDash.Cached
{
    public class AccessLevelCq : ICachedItemQuery
    {
        public List<int> Connectors { get; private set; }
        public string Type { get; private set; }
        public int Timeout { get; set; }

        private const string UsedLevels = @"SELECT SEGMENT.NAME AS NAME, COUNT (ACCESSLVL.DESCRIPT) AS VALUE
       FROM   SEGMENT, ACCESSLVL
       WHERE  ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID 
       GROUP BY SEGMENT.NAME
       ORDER BY VALUE desc";

        private const string EmptyLevels = @"SELECT SEGMENT.NAME AS NAME, COUNT(ACCESSLVID) AS VALUE
       FROM   SEGMENT, ACCESSLVL
       WHERE ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID NOT IN (SELECT ACCESSLVID FROM ACCLVLINK) 
       GROUP BY SEGMENT.NAME
       ORDER BY VALUE desc";

        private const string SingleReaderLevels = @"SELECT SEGMENT.NAME AS NAME, COUNT(ACCESSLVID) AS VALUE
       FROM   SEGMENT, ACCESSLVL
       WHERE ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID IN (SELECT ACCLVLINK.ACCESSLVID FROM ACCLVLINK GROUP BY ACCLVLINK.ACCESSLVID HAVING COUNT(ACCLVLINK.ACCESSLVID)=1) 
       GROUP BY SEGMENT.NAME
       ORDER BY VALUE desc";

        private const string UnAssignedLevels = @" SELECT SEGMENT.NAME AS NAME, COUNT(ACCESSLVID) AS VALUE
       FROM   SEGMENT, ACCESSLVL
       WHERE ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID NOT IN (SELECT ACCLVLID FROM BADGELINK) 
       GROUP BY SEGMENT.NAME
       ORDER BY VALUE desc";

        private List<AccessLevel> _Data = new List<AccessLevel>();

        public AccessLevelCq()
        {
            Connectors = new List<int>() { 2, 3, 4, 5 };
            Type = "AccessLevel";
            Timeout = 150;

        }

      

        public void RunQuery(string connStr, string regionName)
        {
            _Data.Clear();
            var drillDownDict = new Dictionary<string, List<DrillDownPart>>();

            RunQueryPart(connStr, UsedLevels, "Used", ref drillDownDict);
            RunQueryPart(connStr, EmptyLevels, "Empty", ref drillDownDict);
            RunQueryPart(connStr, SingleReaderLevels, "Single", ref drillDownDict);
            RunQueryPart(connStr, UnAssignedLevels, "Unassigned", ref drillDownDict);

            _Data = drillDownDict.Keys.Select(segment => new AccessLevel(segment, drillDownDict[segment])).ToList();

        }

        public object GetData()
        {
            return _Data;
        }

        private void RunQueryPart(string connStr, string sql, string partName, ref Dictionary<string, List<DrillDownPart>> drillDownDict)
        {
            using (var conn = new SqlConnection(connStr))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var name = reader["NAME"].ToString();
                        if (!drillDownDict.ContainsKey(name))
                        {
                            drillDownDict.Add(name, new List<DrillDownPart>());
                        }

                        drillDownDict[name].Add(new DrillDownPart(partName, reader));
                    }
                }
            }
        }
    }
}
