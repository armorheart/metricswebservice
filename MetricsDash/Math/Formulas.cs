﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Math
{
    public static class Formulas
    {
        public static double Percent(int value, int max)
        {
            return (double) value/max*100;
        }
    }
}
