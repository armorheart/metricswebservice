﻿using System;
using System.Collections.Generic;
using System.Data;
using Phase4Lib.Database;

namespace MetricsDash.Models
{
    public class MetricsHistory
    {
        public string Server { get; set; }

        public DateTime? TimeStamp { get; set; }

        public Guid? Series { get; set; }

        public List<MetricShort> Metrics { get; set; }
 
        public MetricsHistory()
        {
            Metrics = new List<MetricShort>();
        }

        public MetricsHistory(IDataRecord record, IEnumerable<StatConfig> stats )
            : this()
        {
            Server = record.GetColumnValue("SERVER", "Master");
            TimeStamp = record.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
            Series = record.GetColumnValue<Guid?>("SERIES_ID");

            //Get the actual stats
            foreach (var config in stats)
            {
                var result = 0;
                if (record.TryGetColumnValue(config.ColumnName, out result))
                {
                    
                    //var result = record.GetColumnValue<int>(config.ColumnName, -1);
                    Metrics.Add(new MetricShort(config, result, Server));
                }
                    
                
            }
        }
        
    }
}