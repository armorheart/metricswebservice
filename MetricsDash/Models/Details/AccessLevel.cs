﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetricsDash.Models.Details
{
    public class AccessLevel
    {
        public string SegmentName { get; set; }
        public int Max { get; set; }
        public int Used { get; set; }
        public int Empty { get; set; }
        public int SingleReader { get; set; }
        public int Unassigned { get; set; }

        public AccessLevel()
        {
            
        }

        public AccessLevel(string segment, IEnumerable<DrillDownPart> values )
        {
            SegmentName = segment;
            foreach (var drillDownPart in values)
            {
                var prop = this.GetType().GetProperty(drillDownPart.Name);
                if( prop != null)
                {
                    prop.SetValue(this, drillDownPart.Value, null);
                }
            }
        }

        
    }
}
