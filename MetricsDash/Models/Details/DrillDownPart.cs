﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Phase4Lib.Database;

namespace MetricsDash.Models.Details
{
    public class DrillDownPart
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public DrillDownPart(string name, IDataReader part)
        {
            Name = name;
            Value = part.GetColumnValue<int>("VALUE", -1);
        }
    }
}
