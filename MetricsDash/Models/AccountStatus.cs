﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Models
{
    public enum AccountStatus
    {
        Inactive = 0,
        Active = 1
    }
}
