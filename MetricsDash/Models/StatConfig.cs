﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using MetricsDash.Repo;

namespace MetricsDash.Models
{
    /// <summary>
    /// Object representing a metric to gather
    /// </summary>
    [Table("UDO_DD2_METRICS_STAT_CONFIG")]
    public class StatConfig : StatConfigBase
    {
        /// <summary>
        /// Minimum threshhold to alert if not met
        /// </summary>
        [Column("MIN_VALUE")]
        [DataMember]
        public int? MinValue { get; set; }

        /// <summary>
        /// Maximum threshold to alert if exceeded
        /// </summary>
        [Column("MAX_VALUE")]
        [DataMember]
        public int? MaxValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("INC_TOLERANCE")]
        [DataMember]
        public int? IncTolerance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("DEC_TOLERANCE")]
        [DataMember]
        public int? DecTolerance { get; set; }

        /// <summary>
        /// 1st date the metric was run
        /// </summary>
        [Column("EARLY_STAT_DATE")]
        [DataMember]
        public DateTime? EarliestStatDate { get; set; }

        /// <summary>
        /// Don't know what this is
        /// </summary>
        [Column("INDV_STAT")]
        [DataMember]
        public bool IndividualStat { get; set; }

        /// <summary>
        /// Whether the metric is enabled
        /// </summary>
        [Column("SYSTEM_METRIC")]
        [DataMember]
        public bool SystemMetric { get; set; }

        public StatConfig() : base()
        {
        }

        
    }
}