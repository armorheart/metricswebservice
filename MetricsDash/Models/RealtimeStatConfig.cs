﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Models
{
    [Table("UDO_DD2_METRICS_REALTIME_STAT_CONFIG")]
    [DataContract]
    public class RealtimeStatConfig : StatConfigBase
    {
        public RealtimeStatConfig()
            : base()
        {
            Values = new List<RealtimeValue>();
        }

        /// <summary>
        /// Minimum threshhold to alert if not met
        /// </summary>
        [Column("MIN_VALUE")]
        [DataMember]
        public int? RealtimeMinValue { get; set; }

        /// <summary>
        /// Maximum threshold to alert if exceeded
        /// </summary>
        [Column("MAX_VALUE")]
        [DataMember]
        public int? RealtimeMaxValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("INC_TOLERANCE")]
        [DataMember]
        public int? RealtimeIncTolerance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("DEC_TOLERANCE")]
        [DataMember]
        public int? RealtimeDecTolerance { get; set; }

        //Query Parts
        [Column("QRY_DATE_FIELD")]
        [DataMember]
        public string DateField { get; set; }

        [Column("QRY_COUNT_FIELD")]
        [DataMember]
        public string CountField { get; set; }

        //Link to Values
        [DataMember]
        public  List<RealtimeValue> Values { get; set; }
    }
}
