﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace MetricsDash.Models
{
    [XmlRoot(Namespace = "MetricsDash.Models")]
    [XmlInclude(typeof(StatConfig))]
    [XmlInclude(typeof(RealtimeStatConfig))]
    [DataContract]
    public class StatConfigBase
    {
        public StatConfigBase()
        {
            Category1Ref = new Category1();
            Category2Ref = new Category2();
            //ServerSelection = new LookupItem();
        }

        /// <summary>
        /// Primary Key
        /// </summary>
        [Key, Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Description of the metric
        /// </summary>
        [Column("DESCRIPTION")]
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Short Display name
        /// </summary>
        [Column("DISPLAY_NAME")]
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// Column Name to Update in UDO_P4_METRICS_HIST
        /// </summary>
        [Column("COLUMN_NAME")]
        [DataMember]
        public string ColumnName { get; set; }

        /// <summary>
        /// Which servers to run the query on
        /// </summary>
        //public LookupItem ServerSelection { get; set; }
        [Column("SERVER_SEL")]
        [DataMember]
        public string ServerSel { get; set; }

        

        //Category Foreign Keys
        public int CATEGORY1 { get; set; }
        public int CATEGORY2 { get; set; }

        /// <summary>
        /// top level category to group metrics together
        /// </summary>
        [DataMember]
        [NotMapped]
        public Category1 Category1Ref { get; set; }

        /// <summary>
        /// Sub category for 2nd level grouping
        /// </summary>
        [DataMember]
        [NotMapped]
        public Category2 Category2Ref { get; set; }

        public int? DATA_TYPE { get; set; }

        [DataMember]
        [NotMapped]
        public StatDataType DataTypeRef { get; set; }

        /// <summary>
        /// SQL query to run to gather the metric
        /// </summary>
        [Column("QUERY")]
        [DataMember]
        public string Query { get; set; }

       

        /// <summary>
        /// Interval to run metric on (daily, weekly, monthly)
        /// </summary>
        [Column("INTERVAL")]
        [DataMember]
        public int Interval { get; set; }

        /// <summary>
        /// Date the metric was run last
        /// </summary>
        [Column("LAST_RUN_DATE")]
        [DataMember]
        public DateTime? LastRunDate { get; set; }

        [Column("STATUS")]
        [DataMember]
        public MetricStatus Status { get; set; }

        [Column("CREATE_DATE")]
        [DataMember]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Validates the ColumnName 
        /// </summary>
        /// <returns></returns>
        public bool ValidateColumnName()
        {
            const string regStr = @"^[a-zA-Z0-9_]*$";
            var r = new System.Text.RegularExpressions.Regex(regStr);
            return r.IsMatch(this.ColumnName);
        }
    }
}