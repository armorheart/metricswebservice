﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetricsDash.Models
{
    public enum MetricStatus
    {
        Enabled = 1,
        Disabled = 2
    }
}
