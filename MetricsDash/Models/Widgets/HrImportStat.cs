﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MetricsDash.Models.Widgets
{
    [Table("UDO_SSBR_HRIMPORT_STATS")]
    public class HrImportStat
    {
        public HrImportStat()
        {
        }

        [Column("TIMESTAMP")]
        public DateTime TimeStamp { get; set; }

        [Column("SERIES_ID")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid SeriesId { get; set; }

        [Column("START_INTERVAL")]
        public DateTime StartInterval { get; set; }

        [Column("END_INTERVAL")]
        public DateTime EndInterval { get; set; }

        [Column("TOTAL")]
        public int Total { get; set; }

        [Column("MISMATCHED")]
        public int MisMatched { get; set; }

        [Column("ADDED")]
        public int Added { get; set; }

        [Column("DEACTIVATED")]
        public int Deactivated { get; set; }

        [Column("TO_NOT_ACTIVE")]
        public int ToNotActive { get; set; }

        [Column("TO_LOA")]
        public int ToLoa { get; set; }

        [Column("FROM_LOA")]
        public int FromLoa { get; set; }

        [Column("TO_CONTRACT")]
        public int ToContract { get; set; }

        [Column("TO_EMPLOYEE")]
        public int ToEmployee { get; set; }

        [Column("AUTO_EXTENDED")]
        public int AutoExtended { get; set; }

        [Column("PHOTOS")]
        public int Photos { get; set; }

        [Column("BADGE_TERM")]
        public int BadgeTerminate { get; set; }

        [Column("BADGE_DEACTIVATE")]
        public int BadgeDeactivate { get; set; }

    }

}
