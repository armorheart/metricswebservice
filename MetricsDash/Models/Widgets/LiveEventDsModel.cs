﻿namespace MetricsDash.Models.Widgets
{
    public class LiveEventDsModel
    {
        public string Name { get; set; }
        public string Connector { get; set; }
        public int Value { get; set; }
        public double Percent { get; set; }

        
    }
}