﻿using System;

namespace MetricsDash.Models.Widgets
{
    public class WidgetOptions
    {
        public int Connector { get; set; }
        public int TopX { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
