﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Models
{
    [Table("UDO_DD2_METRICS_REALTIME_VALUES")]
    [DataContract]
    public class RealtimeValue
    {
        [Column("SERVER", Order = 1), Key]
        [DataMember]
        public string Server { get; set; }

        [Column("STAT_ID", Order = 2), Key]
        [DataMember]
        public int StatId { get; set; }

        [ForeignKey("StatId")]
        public RealtimeStatConfig Stat { get; set; }

        [Column("TIMESTAMP")]
        [DataMember]
        public DateTime? TimeStamp { get; set; }

        [Column("VALUE")]
        [DataMember]
        public int? Value { get; set; }

        [Column("MIN")]
        [DataMember]
        public int? Min { get; set; }

        [Column("MAX")]
        [DataMember]
        public int? Max { get; set; }

        [Column("AVG")]
        [DataMember]
        public int? Avg { get; set; }
    }
}
