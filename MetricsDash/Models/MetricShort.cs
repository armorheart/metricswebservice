using System.Reflection;

namespace MetricsDash.Models
{
    /// <summary>
    /// Model representing a individual metric
    /// </summary>
    public class MetricShort
    {

        public string Server { get; set; }
        public string ColumnName { get; set; }

        public string Description { get; set; }

        public int Value { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public int Avg { get; set; }

        public MetricShort()
        {
            
        }

        public MetricShort(StatConfig config, int result, string server)
        {
            ColumnName = config.ColumnName;
            Description = config.Description;
            Value = result;
            Server = server;
        }

        public MetricShort(RealtimeStatConfig config, int result, string server)
        {
            ColumnName = config.ColumnName;
            Description = config.Description;
            Value = result;
            Server = server;   
        }

        public MetricShort(StatConfig config, int result, int min, int max, int avg, string server)
            : this(config, result, server)
        {
            Min = min;
            Max = max;
            Avg = avg;
        }

        public MetricShort(RealtimeStatConfig config, int result, int min, int max, int avg, string server)
            : this(config, result, server)
        {
            Min = min;
            Max = max;
            Avg = avg;
        }
    }
}