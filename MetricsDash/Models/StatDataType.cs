using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace MetricsDash.Models
{
    [Table("UDO_DD2_METRICS_DATA_TYPE")]
    public class StatDataType
    {
        [Key, Column("ID")]
        [DataMember]
        public int Id { get; set; }

        [Column("TYPE")]
        [DataMember]
        public string Type { get; set; }
    }
}