namespace MetricsDash.Models
{
    public enum Interval
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Quarterly = 4,
        Hourly = 5,
        HalfHourly = 6
    }

    public static class IntervalHelper
    {
        public static bool  InRange(int interval)
        {
            return (interval >= 1 || interval <= 6);
        }
    }
}