﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.Models
{
    public class MetricVm
    {
        public string Name { get; set; }
        public DateTime TimeStamp { get; set; }
        public int Value { get; set; }

        public Dictionary<string, int> Breakdowns { get; set; }

        public bool MultipleBreak { get; set; }
 
        public string Category { get; set; }
        public string Category2 { get; set; }   
        //% change from last interval
        public string Change { get; set; }
        //% change from 1 year ago
        public string YearChange { get; set; }
        public string Health { get; set; }
        public string Tolerance { get; set; }

        public string Status { get; set; }

        public StatConfig Metric { get; set; }

        public MetricVm()
        {
            Metric = new StatConfig();
            Breakdowns = new Dictionary<string, int>();
            MultipleBreak = false;
        }

        public MetricVm(int value, StatConfig metric)
        {
            Name = metric.DisplayName;
            Value = value;
            Metric = metric;

            Category = metric.Category1Ref.Name;
            Category2 = metric.Category2Ref.Name;

            //replace this with logic
            Health = "Normal";
            int dec = metric.DecTolerance ?? 0;
            int inc = metric.IncTolerance ?? 0;

            Tolerance = string.Format("-{0} / +{1}", inc.ToString("p"), dec.ToString("p"));

            Status = metric.Status.ToString();

        }

        public MetricVm(StatConfig metric, int value, int lastValue, int lastYearValue)
            : this(value, metric)
        {
            
            if (value > 0 && lastValue > 0)
            {
                var percent = (double)(value - lastValue)/(double)lastValue;
                Change = string.Format("{0:0.0%}", percent);
            }
            else
            {
                Change = "0.0%";
            }

            if (value > 0 && lastYearValue > 0)
            {
                var percent = (double)(value - lastYearValue) / (double)lastYearValue;
                YearChange = string.Format("{0:0.0%}", percent);
            }
            else
            {
                YearChange = "0.0%";
            }
        }

        public MetricVm(StatConfig metric, int value, int lastValue, int lastYearValue, DateTime timeStamp)
            : this(metric, value, lastValue, lastYearValue)
        {
            TimeStamp = timeStamp;
        }
            
    }
}
