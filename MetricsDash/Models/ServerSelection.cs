﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetricsDash.Models
{
    public enum ServerSelection
    {
        AllServers = 2,
        Master = 3,
        Regions = 4
    }
}
