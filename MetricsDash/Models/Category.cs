using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using MetricsDash.Repo;

namespace MetricsDash.Models
{
    [XmlRoot(Namespace = "MetricsDash.Models")]
    [XmlInclude(typeof(Category1))]
    [XmlInclude(typeof(Category2))]
    public class Category
    {
        [Key, Column("ID")]
        [DataMember]
        public int Id { get; set; }

        [Column("DESCRIPTION")]
        [DataMember]
        public string Name { get; set; }

        [Column("PARENT")]
        [DataMember]
        public int? Parent { get; set; }

        public Category()
        {
            //Id = 0;
            //Name = "None";
        }

       
    }
}