﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Phase4Lib.Database;

namespace MetricsDash.Models
{
    public class HistoryPoint
    {
        public DateTime TimeStamp { get; set; }
        public int Value { get; set; }

    }

    public class SingleHistoryVm
    {
        public string Name { get; set; }
        public string ColumnName { get; set; }

        public Dictionary<string, List<HistoryPoint>> Plots { get; set; }
 
        public SingleHistoryVm()
        {
            Plots = new Dictionary<string, List<HistoryPoint>>();  
        }

        public void AddPoint(string server, HistoryPoint point)
        {
            if (!Plots.ContainsKey(server))
            {
                Plots.Add(server, new List<HistoryPoint>());
            }

            Plots[server].Add(point);
          
        }

        public void AddPoint(IDataRecord record, string columnName)
        {
            var server = record.GetColumnValue<string>("SERVER", "Master");
            var ts = record.GetColumnValue<DateTime>("VARIABLE_BEGIN_INTERVAL");
            var value = record.GetColumnValue<int>(columnName, 0);

            AddPoint(server, new HistoryPoint
                {
                    TimeStamp = ts, 
                    Value = value
                });
        }
    }
}
