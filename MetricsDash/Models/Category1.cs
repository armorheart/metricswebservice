﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Repo;

namespace MetricsDash.Models
{
    [Table("UDO_DD2_METRICS_STAT_CATEGORY1")]
    public class Category1
    {
        [Key, Column("ID")]
        [DataMember]
        public int Id { get; set; }

        [Column("DESCRIPTION")]
        [DataMember]
        public string Name { get; set; }

        [Column("PARENT")]
        [DataMember]
        public int? Parent { get; set; }

        public Category1()
             {}

        //public Category1(ConfigCategory1 cat)
        //{
        //    Id = cat.ID;
        //    Name = cat.DESCRIPTION;
        //    Parent = cat.PARENT;
        //}

    }
}
