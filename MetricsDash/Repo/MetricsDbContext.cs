﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models;
using MetricsDash.UserManagement;

namespace MetricsDash.Repo
{
    public class MetricsDbContext : DbContext
    {
        public MetricsDbContext() {}

        public MetricsDbContext(string connectionName)
            : base(connectionName)
        {
            Set<StatConfig>();
            Set<RealtimeStatConfig>();
            Set<RealtimeValue>();
            Set<Category1>();
            Set<Category2>();
            Set<MetricUser>();
            Set<StatDataType>();
        }

        public DbSet<StatConfig> StatConfigs { get; set; }
        public DbSet<RealtimeStatConfig> RealtimeStatConfigs { get; set; }
        public DbSet<RealtimeValue> RealtimeValues { get; set; }
        public DbSet<Category1> Category1s { get; set; }
        public DbSet<Category2> Category2s { get; set; }
        public DbSet<StatDataType> DataTypes { get; set; }
        public DbSet<MetricUser> MetricUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StatConfig>();
            modelBuilder.Entity<RealtimeStatConfig>();
            modelBuilder.Entity<RealtimeValue>();
            modelBuilder.Entity<Category1>();
            modelBuilder.Entity<Category2>();
            modelBuilder.Entity<StatDataType>();
            modelBuilder.Entity<MetricUser>();
        }
    }
}
