﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mime;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsDash.UserManagement;
using Microsoft.SqlServer.Server;
using Phase4Lib.Database;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace MetricsDash.Repo
{
    public class HistoryRepo : IHistoryRepo
    {
        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _ConnStr;
        private readonly string _ConnectionName;
        private readonly IMetricsRepo<StatConfig> _NewMetricsRepo;
        private readonly IRoleStore _RoleStore;

        public Dictionary<string, ConnectorConfig> Connectors { get; private set; }

        private const string HistorySql = @"SELECT * FROM UDO_DD2_METRICS_HISTORY ";

        private const string LastHistorySql = @"SELECT DISTINCT TOP 2 VARIABLE_BEGIN_INTERVAL 
                    FROM UDO_DD2_METRICS_HISTORY ORDER BY VARIABLE_BEGIN_INTERVAL DESC";

        private const string HistoryRangeYearSql = @"SELECT DISTINCT VARIABLE_BEGIN_INTERVAL 
                    FROM UDO_DD2_METRICS_HISTORY
                    WHERE DATEPART(YYYY, VARIABLE_BEGIN_INTERVAL) = @year
                    ORDER BY VARIABLE_BEGIN_INTERVAL";

        private const string HistoryRangeMonthSql = @"SELECT DISTINCT VARIABLE_BEGIN_INTERVAL 
                    FROM UDO_DD2_METRICS_HISTORY
                    WHERE VARIABLE_BEGIN_INTERVAL >= @startDate
                    AND VARIABLE_BEGIN_INTERVAL <= @endDate
                    ORDER BY VARIABLE_BEGIN_INTERVAL";

        private const string HistoryRangeYearOverYear = @"SELECT DISTINCT VARIABLE_BEGIN_INTERVAL 
                    FROM UDO_DD2_METRICS_HISTORY
                    WHERE DATEPART(MM, VARIABLE_BEGIN_INTERVAL) = @month
                    ORDER BY VARIABLE_BEGIN_INTERVAL";

        

        private const string SingleHistorySql = @"select VARIABLE_BEGIN_INTERVAL, SERVER, SERIES_ID, {0}
                    from UDO_DD2_METRICS_HISTORY
                    WHERE VARIABLE_BEGIN_INTERVAL >= @startDate
                    AND VARIABLE_BEGIN_INTERVAL <= @endDate
                    ORDER BY VARIABLE_BEGIN_INTERVAL";

        //where {0} IS NOT NULL AND 

        private const string SingleYearOverYearSql = @"SELECT VARIABLE_BEGIN_INTERVAL, SERVER, SERIES_ID, {0}
                    from UDO_DD2_METRICS_HISTORY
                    WHERE VARIABLE_BEGIN_INTERVAL IN ({1})
                    ORDER BY VARIABLE_BEGIN_INTERVAL";

        //where {0} IS NOT NULL AND 

        public HistoryRepo(string connStr, string connectionName, 
            IMetricsRepo<StatConfig> metricsRepo, 
            IRoleStore roleStore )
        {
            _ConnStr = connStr;
            _ConnectionName = connectionName;
            ICommonRepo commonRepo = new CommonRepo(connStr);
            Connectors = new Dictionary<string, ConnectorConfig>();
            //Initialize Connectors
            Connectors = commonRepo.GetRegions().ToDictionary(k => k.Id.ToString(), val => val);

            _NewMetricsRepo = metricsRepo;
            _RoleStore = roleStore;

            //Tell the entity framework to not create the database or tables
            System.Data.Entity.Database.SetInitializer<MetricsDbContext>(null);
            System.Data.Entity.Database.SetInitializer<CommonDataContext>(null);
        }

        public List<MetricsHistory> GetHistory()
        {
            return GetHistory(null, null, null, null);
        }

        public List<MetricsHistory> GetHistory(int? category1, int? category2, DateTime? beginDate, DateTime? endDate)
        {
            var history = new List<MetricsHistory>();

            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = GetWhereClause(category1, category2, beginDate, endDate))
                {
                    cmd.Connection = conn;
                    conn.Open();

                    var reader = cmd.ExecuteReader();
                    //Get Metrics to get column names
                    var metrics = _NewMetricsRepo.GetMetrics();

                    while (reader.Read())
                    {
                        history.Add(new MetricsHistory(reader, metrics));
                    }

                }
            }
            return history;
        }


        public IEnumerable<MetricVm> GetLastMetricsTotals()
        {
            var ts = GetLastHistoryDate();
            if (!ts.Any())
            {
                return new List<MetricVm>();
            }

            return GetMetricsTotals(ts.Last(), ts.First());

        }

        public IList<MetricVm> GetLastMetricTotals(string[] columnNames)
        {
            var ts = GetLastHistoryDate();
            if( !ts.Any()) return new List<MetricVm>();

            var values = GetMetricsTotals(ts.Last(), ts.First());
            return values.Where(v => columnNames.Contains(v.Metric.ColumnName)).ToList();
        }

        public IEnumerable<MetricVm> GetMetricsTotals(int year)
        {
            var ts = GetHistoryDates(year);
            if (!ts.Any())
            {
                return new List<MetricVm>();
            }

            return GetMetricsTotals(ts.First(), ts.Last());
        }

        public IEnumerable<MetricVm> GetMetricsTotals(int year, int month)
        {
            var ts = GetHistoryDates(year, month);
            if (!ts.Any())
            {
                return new List<MetricVm>();
            }
            return GetMetricsTotals(ts.First(), ts.Last());
        }

        public IEnumerable<MetricVm> GetMetricsTotals(DateTime startDate, DateTime endDate)
        {
             var dataPoints = GetHistory(null, null, startDate, endDate);

            //Latest Set of datapoints
            var latest = new List<MetricShort>();
            foreach (var latestDp in dataPoints.Where(d => d.TimeStamp == endDate))
            {
                latest.AddRange(latestDp.Metrics);
            }
                
            //The set before that
            var last = new List<MetricShort>();
            foreach (var lastDp in dataPoints.Where(d => d.TimeStamp == startDate))
            {
                last.AddRange(lastDp.Metrics);
            }

            var lastYearDp = GetHistory(null, null, endDate.AddYears(-1), endDate.AddYears(-1).AddMonths(1));
            var lastYear = new List<MetricShort>();
            var lastYearTs = lastYearDp.Max(d => d.TimeStamp);
            foreach (var dp in lastYearDp.Where(d => d.TimeStamp == lastYearTs))
            {
                lastYear.AddRange(dp.Metrics);    
            }

            //List of enabled stats
            var stats = _NewMetricsRepo.GetMetrics().Where(m => m.Status == MetricStatus.Enabled);

            var totals = new List<MetricVm>();
            foreach (var stat in stats)
            {
                int val = 0, lastVal = 0, lastYearVal = 0;
                var breakdowns = new Dictionary<string, int>();

                if (stat.ServerSel.Split(',').Count() > 1)
                {
                    //For a breakpoint, not crazy
                    val = val;
                }

                foreach (var connector in stat.ServerSel.Split(','))
                {
                    if (string.IsNullOrEmpty(connector) || !Connectors.ContainsKey(connector))
                    {
                        continue;
                    }

                    var m =
                        latest.SingleOrDefault(
                            p => p.Server.ToLower() == Connectors[connector].Name.ToLower() && p.ColumnName == stat.ColumnName);

                    if (m != null)
                    {
                        val += m.Value;
                        breakdowns.Add(Connectors[connector].Name, m.Value);
                    }

                    m =
                        last.SingleOrDefault(
                            p => p.Server.ToLower() == Connectors[connector].Name.ToLower() && p.ColumnName == stat.ColumnName);

                    if (m != null)
                    {
                        lastVal += m.Value;
                    }

                    m =
                        lastYear.SingleOrDefault(
                            p => p.Server.ToLower() == Connectors[connector].Name.ToLower() && p.ColumnName == stat.ColumnName);

                    if (m != null)
                    {
                        lastYearVal += m.Value;
                    }

                }
                
                totals.Add(new MetricVm(stat, val, lastVal, lastYearVal, endDate)
                {
                    Breakdowns = breakdowns,
                    MultipleBreak = breakdowns.Keys.Count > 1
                });

                
            }

            return totals;
        }


        

        public IEnumerable<DateTime> GetLastHistoryDate()
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(LastHistorySql, conn))
                {
                    conn.Open();
                    var timeStamps = new List<DateTime>();
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ts = reader.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
                        if (ts != null)
                        {
                            timeStamps.Add((DateTime)ts);
                        }
                    }

                    return timeStamps;
                }
            }
        }

        public IEnumerable<DateTime> GetHistoryDates(int year)
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(HistoryRangeYearSql, conn))
                {
                    conn.Open();
                    var timeStamps = new List<DateTime>();
                    cmd.Parameters.AddWithValue("@year", year);
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ts = reader.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
                        if (ts != null)
                        {
                            timeStamps.Add((DateTime)ts);
                        }
                    }

                    return timeStamps;
                }
            }
        }

        public IEnumerable<DateTime> GetHistoryDates(int year, int month)
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(HistoryRangeMonthSql, conn))
                {
                    conn.Open();
                    var timeStamps = new List<DateTime>();

                    var tmpDate = new DateTime(year, month, 1);
                    var endDate = tmpDate;
                    var startDate = tmpDate.AddMonths(-1);
                    cmd.Parameters.AddWithValue("@startDate", startDate);
                    cmd.Parameters.AddWithValue("@endDate", endDate);
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ts = reader.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
                        if (ts != null)
                        {
                            timeStamps.Add((DateTime)ts);
                        }
                    }

                    return timeStamps;
                }
            }
        }

        public List<DateTime> GetPastYearDates(int startYear, int startMonth)
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(HistoryRangeMonthSql, conn))
                {
                    conn.Open();
                    var timeStamps = new List<DateTime>();

                    var tmpDate = new DateTime(startYear, startMonth, 1);
                    var endDate = tmpDate;
                    var startDate = tmpDate.AddYears(-1);
                    cmd.Parameters.AddWithValue("@startDate", startDate);
                    cmd.Parameters.AddWithValue("@endDate", endDate);
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ts = reader.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
                        if (ts != null)
                        {
                            timeStamps.Add((DateTime)ts);
                        }
                    }

                    return timeStamps;
                }
            }
        }

        public List<DateTime> GetYearOverYearDates(int month)
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(HistoryRangeYearOverYear, conn))
                {
                    conn.Open();
                    var timeStamps = new List<DateTime>();
                    cmd.Parameters.AddWithValue("@month", month);
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var ts = reader.GetColumnValue<DateTime?>("VARIABLE_BEGIN_INTERVAL");
                        if (ts != null)
                        {
                            timeStamps.Add((DateTime)ts);
                        }
                    }

                    return timeStamps;
                }
            }
        }


        public SingleHistoryVm GetSingleHistory(string columnName, int month)
        {
            var timestamps = GetYearOverYearDates(month);
            if (!timestamps.Any())
            {
                return new SingleHistoryVm();
            }

            //get the metric details
            var metric = _NewMetricsRepo.GetMetric(columnName);

            

            //get history since begindate 
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = conn.CreateCommand())
                {
                    conn.Open();
                    var single = new SingleHistoryVm
                    {
                        Name = metric.DisplayName,
                        ColumnName = metric.ColumnName
                    };

                    var parameters = new string[timestamps.Count()];

                    for (var x = 0; x < timestamps.Count(); x++)
                    {
                        parameters[x] = "@p" + x;
                        cmd.Parameters.AddWithValue(parameters[x], timestamps[x]);
                    }

                    cmd.CommandText = string.Format(SingleYearOverYearSql, columnName, string.Join(",", parameters));
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        single.AddPoint(reader, columnName);
                    }

                    return single;
                }
            }

        }

        public SingleHistoryVm GetSingleHistory(string columnName, int year, int month,
            Interval subTotal)
        {
            var ts = GetPastYearDates(year, month);
            if (!ts.Any())
            {
                return new SingleHistoryVm();
            }

            return GetSingleHistory(columnName, ts.First(), ts.Last().AddDays(1), subTotal);
        }

        /// <summary>
        /// Gets the History for a single metric Totaled in Intervals
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="beginDate"></param>
        /// <param name="subTotal"></param>
        public SingleHistoryVm GetSingleHistory(string columnName, DateTime beginDate, DateTime endDate, Interval subTotal )
        {
            //get the metric details
            var metric = _NewMetricsRepo.GetMetric(columnName);

            var sql = string.Format(SingleHistorySql, columnName);

            //get history since begindate 
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    var single = new SingleHistoryVm
                        {
                            Name = metric.DisplayName,
                            ColumnName = metric.ColumnName
                        };

                    cmd.Parameters.AddWithValue("@startDate", beginDate);
                    cmd.Parameters.AddWithValue("@endDate", endDate);
                    

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        single.AddPoint(reader, columnName);
                    }


                    //Remove any connectors that contain all zeros
                    var toRemove = single.Plots.Keys.Where(connector => !single.Plots[connector].Any(v => v.Value > 0)).ToList();

                    foreach (var connector in toRemove)
                    {
                        single.Plots.Remove(connector);
                    }

                    return single;
                }
            }
            
        }

        public List<LookupItem> GetLookupValues(string id)
        {
            switch (id)
            {
                case "Category":

                    using (var context = new MetricsDbContext(_ConnectionName))
                    {
                        return context.Category1s.Select(c1 => new LookupItem
                        {
                            Id = c1.Id,
                            Name = c1.Name
                        }).ToList();
                    }
                    break;
                case "SubCategory":
                    using (var context = new MetricsDbContext(_ConnectionName))
                    {
                        return context.Category2s.Select(c1 => new LookupItem
                        {
                            Id = c1.Id,
                            Name = c1.Name
                        }).ToList();
                    }
                    break;
                case "ConnectorType":
                    using (var context = new CommonDataContext(_ConnectionName))
                    {
                        return context.ConnectorTypes.Select(c1 => new LookupItem
                        {
                            Id = c1.Id,
                            Name = c1.Name
                        }).ToList();
                    }
                    break;
                case "DataType":
                    using (var context = new MetricsDbContext(_ConnectionName))
                    {
                        return context.DataTypes.Select(d => new LookupItem
                        {
                            Id = d.Id,
                            Name = d.Type
                        }).ToList();
                    }
                    break;
                case "AccountStatus":
                    return (from AccountStatus value in Enum.GetValues(typeof (AccountStatus))
                        select new LookupItem()
                        {
                            Id = (int) value,
                            Name = Enum.GetName(typeof (AccountStatus), value)
                        }).ToList();
                
                    break;

                case "StatStatus":
                    return (from MetricStatus value in Enum.GetValues(typeof (MetricStatus))
                        select new LookupItem()
                        {
                            Id = (int) value,
                            Name = Enum.GetName(typeof (MetricStatus), value)
                        }).ToList();
                
                    break;
                case "Permissions":
                    return _RoleStore.GetRoles().Select(r => new LookupItem()
                    {
                        Id = r.PermissionBit,
                        Name = r.Name
                    }).ToList();
            }

            throw new Exception("Lookup Category not found");

        }

        public int InitHistoryTable()
        {
            var columns = _NewMetricsRepo.GetMetrics();
            var added = 0;
            foreach (var column in columns)
            {
                try
                {
                   _NewMetricsRepo.AddColumn(column);
                    _Log.Info(string.Format("Added Column for {0}", column.ColumnName));
                    added++;
                }
                catch (Exception )
                {
                }
            }

            return added;
        }

        

        private SqlCommand GetWhereClause(int? category1, int? category2, DateTime? beginDate, DateTime? endDate)
       {
           var cmd = new SqlCommand();
           var sql = "WHERE ";
           
           var bMuliti = false;

           if (category1 != null)
           {
               bMuliti = true;
               sql += "CATEGORY1 = @category ";
               cmd.Parameters.AddWithValue("@category", category1);
           }

           if (category2 != null)
           {
               if (bMuliti)
               {
                   sql += "AND ";
               }
               bMuliti = true;
               sql += "CATEGORY2 = @cat2 ";
               cmd.Parameters.AddWithValue("@cat2", category2);

           }

           if (beginDate != null)
           {
               if (bMuliti)
               {
                   sql += "AND ";
               }
               bMuliti = true;
               sql += "   VARIABLE_BEGIN_INTERVAL >= @beginDate ";
               cmd.Parameters.AddWithValue("@beginDate", beginDate);
           }

           if (endDate != null)
           {
               if (bMuliti)
               {
                   sql += "AND ";
               }
               bMuliti = true;
               sql += "   VARIABLE_BEGIN_INTERVAL <= @endDate ";
               cmd.Parameters.AddWithValue("@endDate", endDate);
           }


           //nothing was set
           if (sql.Equals("WHERE "))
           {
               sql = HistorySql;
           }
           else
           {
               sql = HistorySql + sql;
           }

           cmd.CommandText = sql;
           return cmd;
       }
        

        
    }

}