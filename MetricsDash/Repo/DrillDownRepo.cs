﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using MetricsDash.Models;
using MetricsDash.Models.Details;
using MetricsDash.Repo.Interfaces;
using Phase4Lib.Database;

namespace MetricsDash.Repo
{
    public class DrillDownRepo : IDrillDownRepo
    {
        private string _ConnStr;

        private const string MaxAccessLevels =
            @"SELECT	SEGMENT.NAME, SEGMENT.MAX_ACLVLS as VALUE 
            FROM SEGMENT WHERE DYNAMIC_SEGMENT_OPTIONS NOT IN (1, 2) 
            ORDER BY SEGMENT.NAME";

        private const string UsedAccessLevels =
            @"SELECT	SEGMENT.NAME, COUNT (ACCESSLVL.DESCRIPT) AS VALUE
	        FROM 	SEGMENT, ACCESSLVL
	        WHERE	ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID 
	        GROUP BY SEGMENT.NAME
	        ORDER BY SEGMENT.NAME";

        private const string EmptyAccessLevels =
            @"SELECT	SEGMENT.NAME, COUNT(ACCESSLVID) AS VALUE
	        FROM	SEGMENT, ACCESSLVL
	        WHERE 	ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID NOT IN (SELECT ACCESSLVID FROM ACCLVLINK) 
	        GROUP BY SEGMENT.NAME
	        ORDER BY SEGMENT.NAME";

        private const string SingleReaderAccessLevels =
            @"SELECT	SEGMENT.NAME, COUNT(ACCESSLVID) AS VALUE
	        FROM	SEGMENT, ACCESSLVL
	        WHERE 	ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID IN (SELECT ACCLVLINK.ACCESSLVID FROM ACCLVLINK GROUP BY ACCLVLINK.ACCESSLVID HAVING COUNT(ACCLVLINK.ACCESSLVID)=1) 
	        GROUP BY SEGMENT.NAME
	        ORDER BY SEGMENT.NAME";

        private const string UnAssignedAccessLevels =
            @"SELECT	SEGMENT.NAME, COUNT(ACCESSLVID) AS VALUE
	        FROM	SEGMENT, ACCESSLVL
	        WHERE 	ACCESSLVL.SEGMENTID = SEGMENT.SEGMENTID AND ACCESSLVID NOT IN (SELECT ACCLVLID FROM BADGELINK) 
	        GROUP BY SEGMENT.NAME
	        ORDER BY SEGMENT.NAME";
        
        public DrillDownRepo(string connStr)
        {
            _ConnStr = connStr;
        }

        public IEnumerable<AccessLevel>  AccessLevels()
        {
            var drillDownDict = new Dictionary<string, List<DrillDownPart>>();
            RunQueryPart(MaxAccessLevels, "Max", ref drillDownDict);
            RunQueryPart(UsedAccessLevels, "Used", ref drillDownDict);
            RunQueryPart(EmptyAccessLevels, "Empty", ref drillDownDict);
            RunQueryPart(SingleReaderAccessLevels, "Single", ref drillDownDict);
            RunQueryPart(UnAssignedAccessLevels, "Unassigned", ref drillDownDict);

            return drillDownDict.Keys.Select(segment => new AccessLevel(segment, drillDownDict[segment])).ToList();

        }

        private void RunQueryPart(string sql, string partName, ref Dictionary<string, List<DrillDownPart>> drillDownDict )
        {
            using( var conn = new SqlConnection(_ConnStr) )
            {
                using (var cmd = new SqlCommand(sql, conn) )
                {
                    conn.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var name = reader["NAME"].ToString();
                        if( !drillDownDict.ContainsKey(name) )
                        {
                            drillDownDict.Add(name, new List<DrillDownPart>());
                        }

                        drillDownDict[name].Add(new DrillDownPart(partName, reader));
                    }
                }
            }
        }


    }
}
