using System;
using System.Collections.Generic;
using MetricsDash.Models;

namespace MetricsDash.Repo.Interfaces
{
    public interface IMetricsRepo<T> where T : StatConfigBase
    {
        void UpdateMetric(T stat, bool isNew);
        void DeleteMetric(string id);
        List<T> GetMetrics();
        T GetMetric(string id);

        /// <summary>
        /// Adds a column to History Table
        /// </summary>
        /// <param name="stat"></param>
        void AddColumn(T stat);

        MetricShort RunMetricQuery(T stat, string region, DateTime timestamp);

        
    }
}