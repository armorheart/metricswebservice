﻿using System;
using System.Collections.Generic;
using MetricsDash.Models;

namespace MetricsDash.Repo.Interfaces
{
    public interface IHistoryRepo
    {
        List<MetricsHistory> GetHistory();
        List<MetricsHistory> GetHistory(int? category1, int? category2, DateTime? beginDate, DateTime? endDate);


        IEnumerable<DateTime> GetLastHistoryDate();
        IEnumerable<MetricVm> GetLastMetricsTotals();
        IEnumerable<MetricVm> GetMetricsTotals(int year);
        IEnumerable<MetricVm> GetMetricsTotals(int year, int month);

        int InitHistoryTable();

        /// <summary>
        /// Gets the History for a single metric Totaled in Intervals
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="beginDate"></param>
        /// <param name="subTotal"></param>
        SingleHistoryVm GetSingleHistory(string columnName, DateTime beginDate, DateTime endDate, Interval subTotal );

        SingleHistoryVm GetSingleHistory(string columnName, int year, int month,
            Interval subTotal);

        SingleHistoryVm GetSingleHistory(string columnName, int month);


        IEnumerable<MetricVm> GetMetricsTotals(DateTime startDate, DateTime endDate);

        List<LookupItem> GetLookupValues(string id);


        IList<MetricVm> GetLastMetricTotals(string[] columnNames);
    }
}