using System.Collections.Generic;
using MetricsDash.Models.Details;

namespace MetricsDash.Repo.Interfaces
{
    public interface IDrillDownRepo
    {
        IEnumerable<AccessLevel>  AccessLevels();
    }
}