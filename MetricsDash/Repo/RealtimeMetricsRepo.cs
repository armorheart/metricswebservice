﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using Phase4Lib.Audit;
using Phase4Lib.Converters;
using Phase4Lib.DataAccess;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace MetricsDash.Repo
{
    public class RealtimeMetricsRepo : IMetricsRepo<RealtimeStatConfig>
    {
        private static readonly ILog _Log = LogManager.GetLogger
           (MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _ConnectionName;
        private readonly IAuditRepo _AuditRepo;

        //private const string AddColumnSql = @"alter table UDO_DD2_METRICS_REALTIME_HISTORY ADD {0} {1} null";

        //Joins the DateField, CountField and Query Part to get the Current Realtime Count
        private const string CountSql = @"SELECT TOP 1 DATEADD(day, DATEDIFF(day, 0, {0}), 0) AS DATE, COUNT({1}) AS CT
                                        {2}
                                        GROUP BY DATEADD(day, DATEDIFF(day, 0, {0}), 0)
                                        ORDER BY DATE DESC";

        //Joins the DateField, CountField and Query Part to get the Current Realtime Avg, Min, and Max
        private const string StatisticsSql = @"SELECT AVG(CT) AS CUR_AVG, MIN(CT) AS CUR_MIN, MAX(CT) AS CUR_MAX
                                        FROM (
                                        SELECT DATEADD(day, DATEDIFF(day, 0, {0}), 0) AS DATE, 
                                        COUNT({1}) AS CT
                                        {2}
                                        GROUP BY DATEADD(day, DATEDIFF(day, 0, {0}), 0) ) AS Q";

        private readonly MetricsDbContext _Context;

        private Dictionary<int, Category1> Cat1Lookup = null;
        private Dictionary<int, Category2> Cat2Lookup = null;
        private Dictionary<int, StatDataType> DtLookup = null; 
        

        public Dictionary<string, ConnectorConfig> Connectors { get; private set; }

        public RealtimeMetricsRepo(string connectionName, IAuditRepo auditRepo)
        {
            _ConnectionName = connectionName;
            _AuditRepo = auditRepo;
            ICommonRepo commonRepo = new CommonRepo(connectionName);
            Connectors = new Dictionary<string, ConnectorConfig>();
            //Initialize Connectors
            Connectors = commonRepo.GetRegions().ToDictionary(k => k.Id.ToString(), val => val);

            //Tell the entity framework to not create the database or tables
            System.Data.Entity.Database.SetInitializer<MetricsDbContext>(null);

            _Context = new MetricsDbContext(_ConnectionName);
            _Context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public void UpdateMetric(RealtimeStatConfig stat, bool updateValues)
        {
            var metric = _Context.RealtimeStatConfigs.SingleOrDefault(m => m.Id == stat.Id);

            if (metric == null)
            {
                try
                {
                    _Context.RealtimeStatConfigs.Add(stat);
                }
                catch (Exception ex)
                {
                    _Log.Error(String.Format("Failed to Update {0}", stat.ColumnName), ex);
                    throw;
                }
            }
            else
            {
                var updater = new LazyObjectUpdater();
                updater.Update(metric, stat, new HashSet<string>() { "Values" });

                if (updateValues)
                {
                    foreach (var value in stat.Values)
                    {
                        var oldVal =
                            _Context.RealtimeValues.SingleOrDefault(v => v.StatId == value.StatId && v.Server == value.Server);

                        if (oldVal == null)
                        {
                            _Context.RealtimeValues.Add(value);
                        }
                        else
                        {
                            updater.Update(oldVal, value);
                        }
                    }    
                }
                
            }

            _Context.SaveChanges();

            //using (var context = new MetricsDbContext(_ConnectionName))
            //{
            //    context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            //    var metric = context.RealtimeStatConfigs.SingleOrDefault(m => m.Id == stat.Id);

            //    if (metric == null)
            //    {
            //        try
            //        {
            //            context.RealtimeStatConfigs.Add(stat);
            //        }
            //        catch (Exception ex)
            //        {
            //            _Log.Error(String.Format("Failed to Update {0}", stat.ColumnName), ex);
            //            throw;
            //        }
            //    }
            //    else
            //    {
            //        //clear values and update them
            //        metric.Values.Clear();
            //        metric.Values.AddRange(stat.Values);

            //        var updater = new LazyObjectUpdater();
            //        updater.Update(metric, stat, new HashSet<string>() { "Values" });

                    
            //    }

            //    context.SaveChanges();
            //}
        }

        public void DeleteMetric(string id)
        {
            var stat = _Context.RealtimeStatConfigs.SingleOrDefault(m => m.ColumnName == id);

            //And remove the values
            var values = _Context.RealtimeValues.Where(v => v.StatId == stat.Id).ToList();
            _Context.RealtimeValues.RemoveRange(values);
            _Context.RealtimeStatConfigs.Remove(stat);
            _Context.SaveChanges();

            //using (var context = new MetricsDbContext(_ConnectionName))
            //{
            //    context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            //    var stat = context.RealtimeStatConfigs.SingleOrDefault(m => m.ColumnName == id);
            //    context.RealtimeStatConfigs.Remove(stat);
            //    context.SaveChanges();
            //}
        }

        public List<RealtimeStatConfig> GetMetrics()
        {
            try
            {
                var metrics = _Context.RealtimeStatConfigs.ToList();
                foreach (var metric in metrics)
                {
                    UpdateLookups(metric);
                    
                    //Now get the values
                    metric.Values = _Context.RealtimeValues.Where(v => v.StatId == metric.Id).ToList();
                }

                return metrics;

                //using (var context = new MetricsDbContext(_ConnectionName))
                //{
                //    context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

                //    var metrics = context.RealtimeStatConfigs.ToList();
                //    foreach (var metric in metrics)
                //    {
                //        UpdateLookups(metric);
                //    }

                //    return metrics;
                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public RealtimeStatConfig GetMetric(string id)
        {
            var metrics = from m in _Context.RealtimeStatConfigs
                          where m.ColumnName == id
                          select m;

            var metric = metrics.SingleOrDefault();
            if (metric != null)
            {
                UpdateLookups(metric);

                //Now get the values
                metric.Values = _Context.RealtimeValues.Where(v => v.StatId == metric.Id).ToList();
            }
            return metric;

            //using (var context = new MetricsDbContext(_ConnectionName))
            //{
            //    context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            //    var metrics = from m in context.RealtimeStatConfigs
            //                  where m.ColumnName == id
            //                  select m;

            //    var metric = metrics.SingleOrDefault();
            //    if (metric != null)
            //    {
            //        UpdateLookups(metric);
            //    }
            //    return metric;
            //}
        }

        public void AddColumn(RealtimeStatConfig stat)
        {
            throw new NotImplementedException();
            //var sql = string.Format(AddColumnSql, stat.ColumnName, stat.DataTypeRef.Type);
            //using (var conn = new SqlConnection(_ConnStr))
            //{
            //    using (var cmd = new SqlCommand(sql, conn))
            //    {
            //        conn.Open();
            //        cmd.ExecuteNonQuery();


            //    }
            //}
        }

        public MetricShort RunMetricQuery(RealtimeStatConfig stat, string region, DateTime timestamp)
        {
            return RunMetricQuery(stat, region, timestamp, GetDatabaseId(Connectors[region].Name));
        }

        /// <summary>
        /// Runs the Metric Query
        /// </summary>
        /// <param name="stat">Metric</param>
        /// <param name="region">Region name or Master</param>
        /// <param name="seriesId"></param>
        /// <param name="timestamp"></param>
        /// <param name="databaseId">Optional Database Id from Lnl_Db</param>
        public MetricShort RunMetricQuery(RealtimeStatConfig stat, string region, DateTime timestamp, int databaseId)
        {
            var retVal = new MetricShort(stat, 0, Connectors[region].Name);
            var connStr = Connectors[region].GetConnectionString();

            var values = new RealtimeValue();
            values.Server = Connectors[region].Name;
            values.StatId = stat.Id;
            values.TimeStamp = timestamp;

            using (var conn = new SqlConnection(connStr))
            {
                conn.Open();

                try
                {
                    //Get the current value on this server
                    var ctActual = string.Format(CountSql, stat.DateField, stat.CountField, stat.Query);
                    using (var cmd = new SqlCommand(ctActual, conn))
                    {
                        cmd.CommandTimeout = 300;
                        var reader = cmd.ExecuteReader();
                        while (reader.HasRows && reader.Read())
                        {
                            retVal.Value = (int)reader["CT"];
                            values.Value = retVal.Value;
                        }

                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                        
                }

                try
                {
                    //And the current min, max, and avg
                    var stActual = string.Format(StatisticsSql, stat.DateField, stat.CountField, stat.Query);
                    using (var cmd = new SqlCommand(stActual, conn))
                    {
                        var reader = cmd.ExecuteReader();

                        //there should only be 1 row
                        while (reader.HasRows && reader.Read())
                        {
                            retVal.Avg = (int)reader["CUR_AVG"];
                            retVal.Min = (int)reader["CUR_MIN"];
                            retVal.Max = (int)reader["CUR_MAX"];
                        }

                        values.Avg = retVal.Avg;
                        values.Min = retVal.Min;
                        values.Max = retVal.Max;

                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                        
                }
            }

            //add the values to the stat
            var found = false;
            foreach (var val in stat.Values.Where(val => val.StatId == values.StatId && val.Server == values.Server))
            {
                found = true;
                //update
                val.TimeStamp = values.TimeStamp;
                val.Value = values.Value;
                val.Min = values.Min;
                val.Max = values.Max;
                val.Avg = values.Avg;
            }

            if (!found)
            {
                stat.Values.Add(values);
            }

            //update the stat with the most current values
            UpdateMetric(stat, true);

            return retVal;
        }

        private void UpdateLookups(RealtimeStatConfig metric)
        {
            if (Cat1Lookup == null)
            {
                Cat1Lookup = _Context.Category1s.ToDictionary(k => k.Id, v => v);
            }
            if (Cat2Lookup == null)
            {
                Cat2Lookup = _Context.Category2s.ToDictionary(k => k.Id, v => v);
            }
            if (DtLookup == null)
            {
                DtLookup = _Context.DataTypes.ToDictionary(k => k.Id, v => v);
            }

            metric.Category1Ref = Cat1Lookup[metric.CATEGORY1];
            metric.Category2Ref = Cat2Lookup[metric.CATEGORY2];
            metric.DataTypeRef = DtLookup[metric.DATA_TYPE ?? 1];
        }

        private DateTime GetBeginInterval(DateTime timestamp, string columnName)
        {
            return timestamp.AddHours(-1);
        }

        /// <summary>
        /// This may be incorrect if the Metrics Database is different then the OnGuard Database
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        private int GetDatabaseId(string region)
        {

            //Get databaseId from Lnl_Db
            using (var context = new OnGuardDataContext(_ConnectionName))
            {
                var dbId = context.LnlDbs.SingleOrDefault(lnl => lnl.NodeName.ToLower() == region.ToLower());
                return dbId == null ? 1 : dbId.Id;
            }
        }
    }
}
