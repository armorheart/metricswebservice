using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using log4net;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using Phase4Lib.Audit;
using Phase4Lib.Converters;
using Phase4Lib.DataAccess;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace MetricsDash.Repo
{
    public class MetricsRepo : IMetricsRepo<StatConfig>
    {
        private static readonly ILog _Log = LogManager.GetLogger
           (MethodBase.GetCurrentMethod().DeclaringType);

        private readonly string _ConnStr;
        private readonly string _ConnectionName;
        private readonly IAuditRepo _AuditRepo;
        private readonly IObjectUpdater _ObjectUpdater;
        private readonly IAuditService _AuditService;


        private const string AddColumnSql = @"alter table UDO_DD2_METRICS_HISTORY ADD {0} {1} null";

        private const string StatisticsSql = @"select AVG({0}) as CUR_AVG, MIN({0}) as CUR_MIN, MAX({0}) as CUR_MAX
                                            from UDO_DD2_METRICS_HISTORY 
                                            where SERVER = @region";

        public Dictionary<string, ConnectorConfig> Connectors { get; private set; }

        private readonly MetricsDbContext _Context;

        private Dictionary<int, Category1> Cat1Lookup = null;
        private Dictionary<int, Category2> Cat2Lookup = null;
        private Dictionary<int, StatDataType> DtLookup = null; 

        public MetricsRepo(string connStr, string connectionName, 
            IObjectUpdater objectUpdater, 
            IAuditService auditService)
        {
            _ConnStr = connStr;
            _ConnectionName = connectionName;
            _ObjectUpdater = objectUpdater;
            _AuditService = auditService;
            ICommonRepo commonRepo = new CommonRepo(connectionName);
            Connectors = new Dictionary<string, ConnectorConfig>();
            //Initialize Connectors
            Connectors = commonRepo.GetRegions().ToDictionary(k => k.Id.ToString(), val => val);

            //Tell the entity framework to not create the database or tables
            System.Data.Entity.Database.SetInitializer<MetricsDbContext>(null);

            _Context = new MetricsDbContext(_ConnectionName);
            _Context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            _AuditService.Init(ConfigurationManager.AppSettings["appName"]);
        }

        public void UpdateMetric(StatConfig stat, bool isNew)
        {
            using (var context = new MetricsDbContext(_ConnectionName))
            {
                var metric = context.StatConfigs.SingleOrDefault(m => m.Id == stat.Id);

                if (metric == null)
                {
                    //Do this in a transaction so we call roll back the insert 
                    //if we can't add the column to the history table
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.StatConfigs.Add(stat);
                            AddColumn(stat);
                            transaction.Commit();

                            _AuditService.AddObject("AddMetric", stat.ColumnName);
                        }
                        catch (Exception ex)
                        {
                            _Log.Error(String.Format("Failed to Update {0}", stat.ColumnName), ex);
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
                else
                {
                    _ObjectUpdater.Update(metric, stat);
                    _AuditService.AddObjectChanges("UpdateMetric", stat.ColumnName, _ObjectUpdater.ReportLastUpdate());   
                }

                context.SaveChanges();
            }
        }

        public void DeleteMetric(string id)
        {
            using (var context = new MetricsDbContext(_ConnectionName))
            {
                var stat = context.StatConfigs.SingleOrDefault(m => m.ColumnName == id);
                context.StatConfigs.Remove(stat);
                context.SaveChanges();

                _AuditService.AddObject("DeleteMetric", id);
                
            }
        }

        public List<StatConfig> GetMetrics()
        {
            try
            {
                using (var context = new MetricsDbContext(_ConnectionName))
                {
                    var metrics = context.StatConfigs.ToList();
                    foreach (var metric in metrics)
                    {
                        UpdateLookups(metric);
                    }

                    return metrics;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public StatConfig GetMetric(string id)
        {
            var metrics = (from m in _Context.StatConfigs
                where m.ColumnName == id
                select m);

            
            var metric = metrics.SingleOrDefault();
            if (metric != null)
            {
                UpdateLookups(metric);
            }

            return metric;


            //using (var context = new MetricsDbContext(_ConnectionName))
            //{
            //    var metrics = from m in context.StatConfigs
            //        where m.ColumnName == id
            //        select m;

            //    return metrics.SingleOrDefault();
            //}
        }

        private void UpdateLookups(StatConfig metric)
        {
            if (Cat1Lookup == null)
            {
                Cat1Lookup = _Context.Category1s.ToDictionary(k => k.Id, v => v);
            }
            if (Cat2Lookup == null)
            {
                Cat2Lookup = _Context.Category2s.ToDictionary(k => k.Id, v => v);
            }
            if (DtLookup == null)
            {
                DtLookup = _Context.DataTypes.ToDictionary(k => k.Id, v => v);
            }
            metric.Category1Ref = Cat1Lookup[metric.CATEGORY1];
            metric.Category2Ref = Cat2Lookup[metric.CATEGORY2];
            metric.DataTypeRef = DtLookup[metric.DATA_TYPE ?? 1];
        }

        public MetricShort RunMetricQuery(StatConfig stat, string region, DateTime timestamp)
        {
            return RunMetricQuery(stat, region, timestamp, GetDatabaseId(Connectors[region].Name));
        }

        /// <summary>
        /// Runs the Metric Query
        /// </summary>
        /// <param name="stat">Metric</param>
        /// <param name="region">Region name or Master</param>
        /// <param name="seriesId"></param>
        /// <param name="timestamp"></param>
        /// <param name="databaseId">Optional Database Id from Lnl_Db</param>
        public MetricShort RunMetricQuery(StatConfig stat, string region, DateTime timestamp, int databaseId)
        {
            var connStr = Connectors[region].GetConnectionString();
            var retVal = new MetricShort(stat, 0, Connectors[region].Name);

            using (var conn = new SqlConnection(connStr))
            {
                conn.Open();

                using (var cmd = new SqlCommand(stat.Query, conn))
                {
                    var startOfMonth = timestamp.Date.AddDays(-timestamp.Day);
                    
                    cmd.CommandTimeout = 300;
                    //Add interval parameters
                    //They'll be ignored if not needed
                    cmd.Parameters.AddWithValue("@BEGIN_INTERVAL", GetBeginInterval(startOfMonth, (Interval)stat.Interval));
                    cmd.Parameters.AddWithValue("@END_INTERVAL", timestamp);
                    cmd.Parameters.AddWithValue("@LNL_DBID", databaseId);

                    var result = (int)cmd.ExecuteScalar();
                    retVal.Value = result;
                }

                
            }

            //Run the statistic query on the Master
            //This might not always succeed. so that's ok
            try
            {
                using (var conn = new SqlConnection(_ConnStr))
                {
                    conn.Open();

                    //Now get the avg, min, max
                    using (var cmd = new SqlCommand(string.Format(StatisticsSql, stat.ColumnName), conn))
                    {
                        cmd.Parameters.AddWithValue("@region", Connectors[region].Name);
                        var reader = cmd.ExecuteReader();

                        //there should only be 1 row
                        while (reader.HasRows && reader.Read())
                        {
                            retVal.Avg = (int)reader["CUR_AVG"];
                            retVal.Min = (int)reader["CUR_MIN"];
                            retVal.Max = (int)reader["CUR_MAX"];
                        }

                        reader.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _Log.Error($"Getting averages for {stat.ColumnName} on {Connectors[region].Name}", e);
            }

            return retVal;
        }

        private DateTime GetBeginInterval(DateTime timestamp, Interval interval)
        {
            switch (interval)
            {
                case Interval.Daily:
                    return timestamp.AddDays(-1);
                case Interval.Weekly:
                    return timestamp.AddDays(-7);
                case Interval.Monthly:
                    return timestamp.AddMonths(-1);
                default:
                    throw new ArgumentOutOfRangeException("interval");
            }
        }

        /// <summary>
        /// Adds a column to History Table
        /// </summary>
        /// <param name="stat"></param>
        public void AddColumn(StatConfig stat)
        {
            var sql = string.Format(AddColumnSql, stat.ColumnName, stat.DataTypeRef.Type);
            using (var conn = new SqlConnection(_ConnStr))
            {
                using (var cmd = new SqlCommand(sql, conn))
                {
                    conn.Open();
                    cmd.ExecuteNonQuery();


                }
            }
        }

        /// <summary>
        /// This may be incorrect if the Metrics Database is different then the OnGuard Database
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        private int GetDatabaseId(string region)
        {

            //Get databaseId from Lnl_Db
            using (var context = new OnGuardDataContext(_ConnectionName))
            {
                var dbId = context.LnlDbs.SingleOrDefault(lnl => lnl.NodeName.ToLower() == region.ToLower());
                return dbId == null ? 1 : dbId.Id;
            }
        }
    }
}