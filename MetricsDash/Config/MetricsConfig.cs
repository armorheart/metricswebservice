﻿using System.Configuration;

namespace MetricsDash.Config
{
    public class MetricsConfig : ConfigurationSection
    {
        public static MetricsConfig GetConfig()
        {
            return ConfigurationManager.GetSection("MetricsConfig") as MetricsConfig;
        }

        //[ConfigurationProperty("Master", IsRequired = true)]
        //public MasterConfig Master
        //{
        //    get { return this["Master"] as MasterConfig; }
        //}

        //[ConfigurationProperty("Regions", IsRequired = false)]
        //public RegionConfig Regions
        //{
        //    get { return (RegionConfig) this["Regions"]; }
        //}

        [ConfigurationProperty("Email", IsRequired = false)]
        public EmailConfig Email
        {
            get { return (EmailConfig) this["Email"]; }
        }



    }

    public class MasterConfig : ConfigurationElement
    {
        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string) this["value"]; }
        }
    }

    public class RegionConfig : ConfigurationElementCollection
    {

        public new RegionItem this[string index]
        {
            get { return base.BaseGet(index) as RegionItem; }
            set
            {
                if (base.BaseGet(index) != null)
                    base.BaseRemove(index);

                this.BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RegionItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RegionItem) element).Name;
        }

        public RegionItem GetItem(int i)
        {
            return base.BaseGet(i) as RegionItem;
        }
    }


    public class RegionItem : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string) this["name"]; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string) this["value"]; }
        }
    }

    public class EmailConfig : ConfigurationElement
    {
        [ConfigurationProperty("From", IsRequired = true)]
        public string From
        {
            get { return (string) this["From"]; }
        }

        [ConfigurationProperty("Subject", IsRequired = true)]
        public string Subject
        {
            get { return (string) this["Subject"]; }
        }

        [ConfigurationProperty("Server", IsRequired = true)]
        public string Server
        {
            get { return (string) this["Server"]; }
        }
    }

}
            