﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace MetricsDash.UserManagement
{
    [Table("UDO_DD2_METRICS_USERS")]
    public class MetricUser : Phase4Lib.Web.Models.User
    {
        [DataMember]
        [NotMapped]
        public AlertPreferences AlertPrefs { get; set; }

        [DataMember]
        [NotMapped]
        public UiPreferences UiPrefs { get; set; }

        public string ALERT_PREF { get; set; }
        public string UI_PREF { get; set; }

        [DataMember]
        [Column("STAT_PERMISSIONS")]
        public string StatPermissions { get; set; } 

        [DataMember]
        [NotMapped]
        public List<Role> PermissionList { get; set; }

        [DataMember]
        [NotMapped]
        public string Status { get; set; }

        public MetricUser()
            :base()
        {
            AlertPrefs = new AlertPreferences();
            UiPrefs = new UiPreferences();
            PermissionList = new List<Role>();
        }

        public void DeserializePrefs()
        {
            AlertPrefs = new AlertPreferences(ALERT_PREF);
            UiPrefs = new UiPreferences(UI_PREF);
        }

        public void SerializePrefs()
        {
            ALERT_PREF = AlertPrefs.ToJson();
            UI_PREF = UiPrefs.ToJson();
        }
    }
}