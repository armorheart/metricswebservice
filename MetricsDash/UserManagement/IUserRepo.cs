using System.Collections.Generic;
using MetricsDash.Models;

namespace MetricsDash.UserManagement
{
    public interface IUserRepo
    {
        IEnumerable<MetricUser> GetUsers();
        MetricUser GetUser(string userId);
        MetricUser UpdateUser(MetricUser user);
        void DeleteUser(int id);
        IList<MetricUser> GetAlertSubscribers(RealtimeStatConfig metric);
    }
}