﻿using System;
using System.Collections.Generic;

namespace MetricsDash.UserManagement
{
    public class AlertPreference : IEquatable<AlertPreference>
    {
        /// <summary>
        /// The column name of the metric the user is subscribing to
        /// </summary>
        public string  SubscriptionId { get; set; }

        public AlertPreference() {}

        public AlertPreference(string name)
        {
            SubscriptionId = name;
        }

        public bool Equals(AlertPreference other)
        {
            return this.SubscriptionId.Equals(other.SubscriptionId, StringComparison.CurrentCultureIgnoreCase);
        }
    }

    public class AlertPreferences
    {
        public List<AlertPreference> Preferences { get; set; } 

        public AlertPreferences()
        {
            Preferences = new List<AlertPreference>();
        }

        public AlertPreferences(string jsonPrefs)
        {
            if (string.IsNullOrWhiteSpace(jsonPrefs))
            {
                return;
            }

            try
            {
                Newtonsoft.Json.JsonConvert.DeserializeObject<AlertPreferences>(jsonPrefs);
            }
            catch (Exception)
            {

            }
        }

        public string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}