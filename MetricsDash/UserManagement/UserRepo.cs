using System;
using System.Collections.Generic;
using System.Linq;
using MetricsDash.Models;
using MetricsDash.Repo;
using Phase4Lib.Converters;

namespace MetricsDash.UserManagement
{
    public class UserRepo : IUserRepo
    {
        private readonly string _ConnStr;

        public UserRepo(string connStr)
        {
            _ConnStr = connStr;

            //Tell the entity framework to not create the database or tables
            System.Data.Entity.Database.SetInitializer<MetricsDbContext>(null);
        }

        public IEnumerable<MetricUser> GetUsers()
        {
            using (var context = new MetricsDbContext(_ConnStr))
            {
                var users = context.MetricUsers;
                foreach (var metricUser in users)
                {
                    metricUser.Status = metricUser.AccountStatus?.ToString();
                }
                return users.ToList();
            }
        }

        public MetricUser GetUser(string userId)
        {
            using (var context = new MetricsDbContext(_ConnStr))
            {
                var user = context.MetricUsers.SingleOrDefault(u => u.UserId == userId);
                return user ?? null;
            }
        }

        public MetricUser UpdateUser(MetricUser user)
        {
            using (var context = new MetricsDbContext(_ConnStr))
            {
                var metricUser = context.MetricUsers.SingleOrDefault(u => u.UserId == user.UserId);

                try
                {
                    if (metricUser == null)
                    {
                        //Add new User
                        SetDefault(user);
                        context.MetricUsers.Add(user);
                    }
                    else
                    {
                        var updater = new LazyObjectUpdater();
                        updater.Update(metricUser, user);
                    }

                    context.SaveChanges();

                    return metricUser ?? user;
                }
                catch (Exception ex)
                {
                    //Log
                    throw;
                }
                
            }
        }

        public void DeleteUser(int id)
        {
            using (var context = new MetricsDbContext(_ConnStr))
            {
                var metricUser = context.MetricUsers.SingleOrDefault(u => u.Id == id);
                try
                {
                    if( metricUser == null)
                        return;

                    context.MetricUsers.Remove(metricUser);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //Log
                    throw;
                }
            }
        }

        public IList<MetricUser> GetAlertSubscribers(RealtimeStatConfig metric)
        {
            var allUsers = GetUsers();

            var alert = new AlertPreference(metric.ColumnName);
            var subUsers = allUsers.Where(u => u.AlertPrefs.Preferences.Contains(alert));
            return subUsers.ToList();
        }

        private void SetDefault(MetricUser user)
        {
            if (string.IsNullOrEmpty(user.StatPermissions))
            {
                user.StatPermissions = "[]";
            }
        }

        
    }
}