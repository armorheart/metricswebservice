﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetricsDash.UserManagement
{
    public class Role
    {
        public string Name { get; set; }
        public int PermissionBit { get; set; }

        public Role() { }

        public Role(string name, int bit)
        {
            Name = name;
            PermissionBit = bit;
        }
    }
}
