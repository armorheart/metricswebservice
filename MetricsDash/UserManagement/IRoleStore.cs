﻿using System.Collections.Generic;

namespace MetricsDash.UserManagement
{
    public interface IRoleStore
    {
        List<Role> GetRoles();
        bool IsUserInRole(Role role, int userBits);
    }
}