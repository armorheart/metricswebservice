﻿using System;
using System.Collections.Generic;

namespace MetricsDash.UserManagement
{
    public class UiPreference
    {
        
    }

    public class UiPreferences
    {
        public List<UiPreference> Preferences { get; set; }
 
        public UiPreferences()
        {
            Preferences = new List<UiPreference>();
        }

        public UiPreferences(string jsonPrefs)
        {
            if (string.IsNullOrWhiteSpace(jsonPrefs))
            {
                return;
            }

            try
            {
                Newtonsoft.Json.JsonConvert.DeserializeObject<UiPreferences>(jsonPrefs);
            }
            catch (Exception)
            {
                
            }
            
        }

        public string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

    }
}