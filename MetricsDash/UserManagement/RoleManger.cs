﻿using System.Collections.Generic;
using System.Data.Entity.Migrations.Builders;
using System.Linq;
using Phase4Lib.Web.Models;

namespace MetricsDash.UserManagement
{
    public class RoleManager : IRoleManager
    {
        private readonly IRoleStore _RoleStore;

        public RoleManager(IRoleStore roleStore)
        {
            _RoleStore = roleStore;
        }

        public IList<Role> GetRolesForUser(User user)
        {
            int perms = user.Permissions ?? 0;
            return _RoleStore.GetRoles().Where(role => _RoleStore.IsUserInRole(role, perms)).ToList();
        }


    }
}