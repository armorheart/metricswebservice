﻿using System.Collections.Generic;
using Phase4Lib.Web.Models;

namespace MetricsDash.UserManagement
{
    public interface IRoleManager
    {
        IList<Role> GetRolesForUser(User user);
    }
}