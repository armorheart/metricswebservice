﻿using System.Collections.Generic;

namespace MetricsDash.UserManagement
{
    public class RoleStore : IRoleStore
    {
        public List<Role> GetRoles()
        {
            return new List<Role>()
            {
                //new Role("View", 1),
                new Role("User",2),
                new Role("Admin", 32)
            };
        }

        public bool IsUserInRole(Role role, int userBits)
        {
            return (role.PermissionBit & userBits) == role.PermissionBit;
        }
    }
}