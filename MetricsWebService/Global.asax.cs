﻿using System;
using System.Web.Http;
using MetricsWebServiceV2;



namespace MetricsWebService
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalHttpFilters(GlobalConfiguration.Configuration.Filters);
        }

        
    }
}
