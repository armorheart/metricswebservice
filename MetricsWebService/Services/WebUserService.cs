﻿using System.Threading;
using System.Web;
using MetricsDash.UserManagement;
using Phase4Lib.Audit;

namespace MetricsWebService.Services
{
    public class WebUserService : IUserService
    {
        private readonly IUserRepo _Repo;

        public WebUserService(IUserRepo repo)
        {
            _Repo = repo;
        }

        public int GetLoggedInUser()
        {

            var u = Thread.CurrentPrincipal.Identity.Name;

            if (!string.IsNullOrEmpty(u))
            {
                return _Repo.GetUser(u).Id;    
            }

            return 0;

        }
    }
}
