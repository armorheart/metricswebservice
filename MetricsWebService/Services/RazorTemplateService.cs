﻿using System.Globalization;
using System.IO;
using System.Web;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.Services
{
    public class RazorTemplateService : ITemplatesService
    {
        private readonly ITemplateEngine _Engine;

        private const string ViewFolder = "~/Templates";

        public RazorTemplateService(ITemplateEngine engine)
        {
            _Engine = engine;
        }

        public string Parse(string templateName, dynamic model, CultureInfo cultureInfo = null)
        {
            var fullPath = LoadView(templateName);
            return _Engine.Parse(fullPath, model);
        }

        private string LoadView(string name)
        {
            var fullPath = Path.Combine(GetCurrentDirectory(), name + ".cshtml");
            var view = File.ReadAllText(fullPath);
            return view;
        }

        private string GetCurrentDirectory()
        {
            return HttpContext.Current.Server.MapPath(ViewFolder);
        }
    }
}