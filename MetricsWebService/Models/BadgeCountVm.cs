﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Phase4Lib.Database;

namespace MetricsWebService.Models
{
    public class BadgeCountVm
    {
        [AdoColumn("BADGEKEY")]
        public int BadgeKey { get; set; }

        [AdoColumn("BADGEID")]
        public long BadgeId { get; set; }

        [AdoColumn("TYPE")]
        public string Type { get; set; }

        [AdoColumn("STATUS")]
        public string Status { get; set; }

        [AdoColumn("BADGING_REASON")]
        public string BadgingReason { get; set; }

        [AdoColumn("CYLINDER")]
        public string Cylinder { get; set; }

        [AdoColumn("BADGING_LOCATION")]
        public string BadgingLocation { get; set; }

        [AdoColumn("CASE")]
        public string Case { get; set; }

        [AdoColumn("PRINT_COUNT")]
        public int Prints { get; set; }

        [AdoColumn("PRINT_DATE")]
        public DateTime PrintDate { get; set; }

    }
}