﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricsWebService.Models
{
    public class AllRegionCountVm
    {
        /// <summary>
        /// Metric Column Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Metric Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<int> Counts { get; set; }

        public AllRegionCountVm()
        {
            Counts = new List<int>();
        }
    }
}