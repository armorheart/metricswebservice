﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricsWebService.Models
{
    public class ServerVm
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Server { get; set; }
        public string DatabaseName { get; set; }
        public int Port { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
    }
}