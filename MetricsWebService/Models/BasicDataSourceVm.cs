﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace MetricsWebService.Models
{
    public class BasicDataSourceVm
    {
        public string Description { get; set; }
        public string Count { get; set; }

         public BasicDataSourceVm(string desc, string count)
        {
            Description = desc;
            Count = count;
        }

         public BasicDataSourceVm() { }
    }
}