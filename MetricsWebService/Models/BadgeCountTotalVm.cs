﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.Components.DictionaryAdapter;

namespace MetricsWebService.Models
{
    public class BadgeCountTotalVm
    {
        public List<BasicDataSourceVm> TypeCounts { get; set; }

        public List<BasicDataSourceVm> LocationCounts { get; set; }

        public List<BasicDataSourceVm> ReasonCounts { get; set; }

        public BadgeCountTotalVm()
        {
            TypeCounts = new List<BasicDataSourceVm>();
            LocationCounts = new List<BasicDataSourceVm>();
            ReasonCounts = new List<BasicDataSourceVm>();
        }
    }
}