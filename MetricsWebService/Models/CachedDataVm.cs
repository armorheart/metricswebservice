﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetricsWebService.Models
{
    public class CachedDataVm
    {
        public int ConnectorId { get; set; }
        public string ConnectorName { get; set; }

        public object Data { get; set; }
    }
}