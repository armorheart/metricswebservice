﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Math;
using MetricsDash.Models.Widgets;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.DataSources.Interfaces;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class MetricsDataSource : IDataSource
    {
        private readonly ITemplatesService _TemplatesService;
        private readonly IHistoryRepo _Repo;
        public string Name { get; private set; }

        public MetricsDataSource(ITemplatesService templatesService, IHistoryRepo repo)
        {
            _TemplatesService = templatesService;
            _Repo = repo;
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var metrics = _Repo.GetLastMetricsTotals();

            var vmList = new List<LiveEventDsModel>();
            //Get metrics that are out of Tolerance
            foreach (var stat in metrics)
            {
                //Ignore stats that haven't had a max value set
                if (stat.Metric.MaxValue == null || stat.Metric.MaxValue == 0)
                {
                    continue;
                }

                foreach (var val in stat.Breakdowns)
                {
                    var vm = new LiveEventDsModel();
                    vm.Name = stat.Metric.DisplayName;
                    vm.Connector = val.Key;
                    vm.Value = val.Value;
                    //value / max * 100
                    vm.Percent = Formulas.Percent(val.Value, stat.Metric.MaxValue ?? 1);
                    //add it to the list
                    vmList.Add(vm);
                }
            }

            var retVal = new List<object>();
            retVal.AddRange(vmList.Where(p => p.Percent >= 100).OrderByDescending(p => p.Percent));
            return retVal;
        }
    }
}