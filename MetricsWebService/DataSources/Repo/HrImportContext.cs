﻿using System.Data.Entity;
using MetricsDash.Models.Widgets;

namespace MetricsWebService.DataSources.Repo
{
    public class HrImportContext : DbContext
    {
        public HrImportContext(string connectionName)
            : base(connectionName)
        {
            Set<HrImportStat>();
        }

        public DbSet<HrImportStat> HrImportStats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HrImportStat>();
        }
    }
}
