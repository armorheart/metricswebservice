﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MetricsWebService.Models;
using Phase4Lib.Database;

namespace MetricsWebService.DataSources.Repo
{
    public class BadgeCountRepo : IBadgeCountRepo
    {
        private const string RawDataSql = @"SELECT	BADGE.BADGEKEY AS [BADGEKEY],
		BADGE.ID AS [BADGEID],
		BADGETYP.NAME AS [TYPE],
		BADGSTAT.NAME AS [STATUS],
		BADGING_REASON.NAME AS [BADGING_REASON],
		(CASE (ltrim(left(BADGING_STATION.NAME,Charindex(' ',BADGING_STATION.NAME)-1))) WHEN 'Cyl1' THEN 'Cylinder1' WHEN 'Cyl1' THEN 'Cylinder2' WHEN 'Cyl3' THEN 'Cylinder3' WHEN 'Cyl4' THEN 'Cylinder4' ELSE 'OTHER' END) AS [CYLINDER],
		BADGING_STATION.NAME AS [BADGING_LOCATION],
		BADGE.BADGING_CASE AS [CASE], 
		BADGE.PRINTS AS [PRINT_COUNT],
		BADGE.LASTPRINT AS [PRINT_DATE]

FROM	BADGE
		INNER JOIN BADGETYP ON (BADGE.TYPE = BADGETYP.ID)
		INNER JOIN BADGSTAT ON (BADGE.STATUS = BADGSTAT.ID)
		LEFT OUTER JOIN BADGING_STATION ON (BADGE.BADGING_STATION = BADGING_STATION.ID)
		LEFT OUTER JOIN BADGING_REASON ON (BADGE.BADGING_REASON = BADGING_REASON.ID)

WHERE	BADGE.LASTPRINT >= @StartDate
		AND BADGE.LASTPRINT <= @EndDate
		AND BADGING_STATION.ID != 0";

        private const string BadgeTypeSql = @"SELECT	BADGETYP.NAME FROM BADGETYP ORDER BY BADGETYP.NAME";

        private const string LocationSql =
            @"SELECT	BADGING_STATION.NAME FROM BADGING_STATION ORDER BY BADGING_STATION.NAME";

        private const string ReasonSql = @"SELECT	BADGING_REASON.NAME FROM BADGING_REASON ORDER BY BADGING_REASON.NAME";

        private readonly string _ConnStr;
        private readonly IDataReaderToClassMapper _DataReaderToClassMapper;

        private static Dictionary<string, string> _Categories = new Dictionary<string, string>()
        {
            {"Type", BadgeTypeSql},
            {"BadgingLocation", LocationSql},
            {"BadgingReason", ReasonSql}
        };

        private List<BadgeCountVm> _Data;

        public BadgeCountRepo(string connStr, IDataReaderToClassMapper dataReaderToClassMapper)
        {
            _ConnStr = connStr;
            _DataReaderToClassMapper = dataReaderToClassMapper;
            _Data = new List<BadgeCountVm>();
        }

        public async Task InitRawDataAsync(DateTime startDate, DateTime endDate)
        {
            _Data.Clear();

            using (var conn = new SqlConnection(_ConnStr))
            {
                await conn.OpenAsync();

                using (var cmd = new SqlCommand(RawDataSql, conn))
                {
                    cmd.Parameters.AddWithValue("@StartDate", startDate);
                    cmd.Parameters.AddWithValue("@EndDate", endDate);

                    var reader = await cmd.ExecuteReaderAsync();

                    while (reader.HasRows && await reader.ReadAsync())
                    {
                        _Data.Add(_DataReaderToClassMapper.MapFromDataReader<BadgeCountVm>(reader));
                    }

                }
            }
        }

        public async Task<IList<string>> GetCategory(string name)
        {
            using (var conn = new SqlConnection(_ConnStr))
            {
                await conn.OpenAsync();

                using (var cmd = new SqlCommand(_Categories[name], conn))
                {
                    var reader = await cmd.ExecuteReaderAsync();

                    var data = new List<string>();

                    while (reader.HasRows && await reader.ReadAsync())
                    {
                        data.Add(reader.GetColumnValue<string>("NAME"));
                    }

                    return data;
                }
            }
        }

        public async Task<Dictionary<string, int>> GetCounts(string field)
        {
            var categories = await GetCategory(field);
            return await GetCounts(categories, field);
        }

        public async Task<Dictionary<string, int>> GetCounts(IList<string> categories, string field)
        {
            return await Task<Dictionary<string, int>>.Factory.StartNew(() =>
            {
                //initialize dictionary
                var counts = categories.ToDictionary(k => k, v => 0);
                var prop = typeof (BadgeCountVm).GetProperty(field);

                Parallel.ForEach(_Data, vm =>
                {
                    var tmpVal = prop.GetValue(vm).ToString();
                    if( !counts.ContainsKey(tmpVal) ) counts.Add(tmpVal, 0);
                    counts[tmpVal]++;    
                });
                return counts;
            });
        }

    }
}