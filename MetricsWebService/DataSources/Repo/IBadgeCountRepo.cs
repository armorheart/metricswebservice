using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MetricsWebService.Models;

namespace MetricsWebService.DataSources.Repo
{
    public interface IBadgeCountRepo
    {
        Task InitRawDataAsync(DateTime startDate, DateTime endDate);
        Task<IList<string>> GetCategory(string name);
        Task<Dictionary<string, int>> GetCounts(string field);
        Task<Dictionary<string, int>> GetCounts(IList<string> categories, string field);
    }
}