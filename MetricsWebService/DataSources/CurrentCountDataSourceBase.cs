using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Cached;
using MetricsWebService.Models;
using Newtonsoft.Json;
using Phase4Lib.Web.DataAccess;

namespace MetricsWebService.DataSources
{
    public class CurrentCountDataSourceBase
    {
        private string _connName;

        public CurrentCountDataSourceBase(string connName)
        {
            _connName = connName;
        }

        protected async Task<List<object>> GetCurrentCounts(string countType)
        {
            var commonRepo = new CommonRepo(_connName);

            var allCounts = await commonRepo.GetCachedDataAsync("Metrics", countType);

            var retVal = new Dictionary<string, AllRegionCountVm>();
            foreach (var regionCounts in allCounts)
            {
                var raw = JsonConvert.DeserializeObject<List<BasicCqModel>>(regionCounts.Data);

                foreach (BasicCqModel count in raw)
                {
                    if (!retVal.ContainsKey(count.Name))
                    {
                        retVal.Add(count.Name, new AllRegionCountVm()
                        {
                            Name = count.Name,
                            Description = count.Description
                        });
                    }

                    retVal[count.Name].Counts.Add(count.Count);
                }
            }

            return retVal.Select(item => item.Value).Cast<object>().ToList();
        }
    }
}