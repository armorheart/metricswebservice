﻿using MetricsWebService.DataSources.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MetricsDash.Models.Widgets;
using System.Threading.Tasks;
using MetricsWebService.Models;
using System.Data.SqlClient;

namespace MetricsWebService.DataSources
{
    public class SpatialDataSource : IDataSource
    {
        private const string Theaters = @"SELECT COUNT(DISTINCT(THEATER_NAME)) AS Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string Regions = @"SELECT COUNT(DISTINCT(REGION_NAME)) AS Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string Countries = @"SELECT COUNT(DISTINCT(COUNTRY_NAME)) AS Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string Campus = @"SELECT COUNT(DISTINCT(CAMPUS_NAME)) AS  Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string Sites = @"SELECT COUNT(DISTINCT(SITE_NAME)) AS Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string Buildings = @"SELECT COUNT(DISTINCT(BLDG_NAME)) AS Count FROM UDO_WPR_SPATIAL_BUILDING WHERE STATUS = 'Online'";
        private const string OnGuardBuildings = @"SELECT COUNT (DISTINCT(ACCESSPANE.UDO_BUILDING_WPR_CODE)) AS Count 
FROM UDO_WPR_SPATIAL_BUILDING LEFT OUTER JOIN ACCESSPANE ON (UDO_WPR_SPATIAL_BUILDING.BLDG_ID = ACCESSPANE.UDO_BUILDING_WPR_CODE)";

        private readonly string _ConnStr;

        private List<Tuple<string, string>> _Queries = new List<Tuple<string, string>>
        {
            new Tuple<string, string>("Theaters", Theaters),
            new Tuple<string, string>("Regions", Regions),
            new Tuple<string, string>("Countries", Countries),
            new Tuple<string, string>("Campuses", Campus),
            new Tuple<string, string>("Sites", Sites),
            new Tuple<string, string>("Buildings", Buildings),
            new Tuple<string, string>("Buildings w/ Lenel", OnGuardBuildings)
        };


        public string Name
        {
            get
            {
                return "Spatial";
            }
        }

        public SpatialDataSource(string connStr)
        {
            _ConnStr = connStr;
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var retVal = new List<object>();
            for (var x = 0; x < _Queries.Count; x++)
            {
                var result = await RunQuery(_Queries[x].Item1, _Queries[x].Item2);
                retVal.Add(result);
            }
            
            return retVal;
        }

        private async Task<BasicDataSourceVm> RunQuery(string name, string sql)
        {
            try
            {
                using (var conn = new SqlConnection(_ConnStr))
                {
                    await conn.OpenAsync();

                    using (var cmd1 = new SqlCommand(sql, conn))
                    {
                        var value = await cmd1.ExecuteScalarAsync();
                        return new BasicDataSourceVm(name, value.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new BasicDataSourceVm();
            }
        }
    }
}