﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MetricsDash.Cached;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Models;
using Newtonsoft.Json;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace MetricsWebService.DataSources
{
    public class RegionCount
    {
        public string Name { get; set; }

        public string MetricName { get; set; }
        public int Count { get; set; }
    }

    public class HardwareCurrentCountDataSource : CurrentCountDataSourceBase, IDataSource
    {
        public string Name { get; }

        public HardwareCurrentCountDataSource(string connName) : base(connName)
        {
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            try
            {
                return await GetCurrentCounts("HardwareCurrentCount");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
    }

    
}