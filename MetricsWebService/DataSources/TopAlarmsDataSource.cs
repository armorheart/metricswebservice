﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Cached;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Models;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class TopAlarmsDataSource : IDataSource
    {
        private readonly string _ConnectionName;
        private readonly ITemplatesService _TemplatesService;
        public string Name { get; private set; }

        public TopAlarmsDataSource(string connectionName, ITemplatesService templatesService)
        {
            _ConnectionName = connectionName;
            _TemplatesService = templatesService;
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var commonRepo = new CommonRepo(_ConnectionName);
            var regions = new List<CachedData>();

            if (options.Connector == 0)
            {
                regions = await commonRepo.GetCachedDataAsync("Metrics", "TopAlarms");
            }
            else
            {
                var combinedData = await commonRepo.GetCachedDataAsync("Metrics", "TopAlarms", options.Connector);
                if (combinedData != null)
                {
                    regions.Add(combinedData);    
                }
            }
            

            if (!regions.Any())
            {
                return new List<object>();
            }

            var retVal = new List<object>();

            foreach (var combinedData in regions)
            {
                var rawData =
                await
                    Task<List<BasicCqModel>>.Factory.StartNew(
                        () => Newtonsoft.Json.JsonConvert.DeserializeObject<List<BasicCqModel>>(combinedData.Data));

                if (options.TopX > 0)
                {
                    retVal.Add(new CachedDataVm()
                    {
                        ConnectorId = combinedData.ConnectorId,
                        Data = rawData.Take(options.TopX)
                    }
                        );
                }
                else
                {
                    retVal.Add(new CachedDataVm()
                    {
                        ConnectorId = combinedData.ConnectorId,
                        Data = rawData
                    }
                        );
                }
            }

            
            return retVal;
        }
    }
}