﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.DataSources.Repo;
using MetricsWebService.Models;

namespace MetricsWebService.DataSources
{
    public class BadgeCountDataSource : IDataSource
    {
        private readonly IBadgeCountRepo _BadgeCountRepo;

        public BadgeCountDataSource(IBadgeCountRepo badgeCountRepo)
        {
            Name = "BadgeCount";

            _BadgeCountRepo = badgeCountRepo;
        }


        public string Name { get; private set; }
        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            await _BadgeCountRepo.InitRawDataAsync(options.StartDate, options.EndDate);

            //Get Type counts
            var task1 = _BadgeCountRepo.GetCounts("Type");
            //Get Location Counts
            var task2 = _BadgeCountRepo.GetCounts("BadgingLocation");
            //Get Reason Counts
            var task3 = _BadgeCountRepo.GetCounts("BadgingReason");

            await Task.WhenAll(task1, task2, task3);

            var allTogetherNow = new BadgeCountTotalVm();

            allTogetherNow.TypeCounts = task1.Result.Select(ct => new BasicDataSourceVm(ct.Key, ct.Value.ToString())).ToList();
            allTogetherNow.LocationCounts = task2.Result.Select(ct => new BasicDataSourceVm(ct.Key, ct.Value.ToString())).ToList();
            allTogetherNow.ReasonCounts = task3.Result.Select(ct => new BasicDataSourceVm(ct.Key, ct.Value.ToString())).ToList();

            return new List<object>() {allTogetherNow};
        }
    }
}
