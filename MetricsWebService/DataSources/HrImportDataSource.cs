﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.DataSources.Repo;
using MetricsWebService.Models;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class HrImportDataSource : IDataSource
    {
        private readonly string _ConnectionName;
        private readonly ITemplatesService _TemplatesService;

        public string Name { get; protected set; }
        

        public HrImportDataSource(string connectionName, ITemplatesService templatesService)
        {
            Name = "HrImport";

            _ConnectionName = connectionName;
            _TemplatesService = templatesService;
            System.Data.Entity.Database.SetInitializer<CommonDataContext>(null);
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            using (var context = new HrImportContext(_ConnectionName))
            {
                //Get Latest HrImport run
                var hr = await context.HrImportStats.OrderByDescending(h => h.TimeStamp).FirstOrDefaultAsync();

                var retVal = new List<object>();
                if (hr != null)
                {
                    retVal.Add(new BasicDataSourceVm("Last Successful Run", hr.TimeStamp.ToString()));
                    retVal.Add(new BasicDataSourceVm("Records Processed", hr.Total.ToString()));
                    retVal.Add(new BasicDataSourceVm("Mismatched Records", hr.MisMatched.ToString()));
                    retVal.Add(new BasicDataSourceVm("Added Records", hr.Added.ToString()));
                    retVal.Add(new BasicDataSourceVm("Terminated Records", hr.ToNotActive.ToString()));
                    retVal.Add(new BasicDataSourceVm("Employee to Contractor", hr.ToContract.ToString()));
                    retVal.Add(new BasicDataSourceVm("Contractor to Employee", hr.ToEmployee.ToString()));
                    retVal.Add(new BasicDataSourceVm("Going on LOA", hr.ToLoa.ToString()));
                    retVal.Add(new BasicDataSourceVm("Returning From LOA", hr.FromLoa.ToString()));
                    retVal.Add(new BasicDataSourceVm("Photos Exported", hr.Photos.ToString()));
                    retVal.Add(new BasicDataSourceVm("Auto Badge Extension", hr.AutoExtended.ToString()));
                    retVal.Add(new BasicDataSourceVm("Badge Termination Audit", hr.BadgeTerminate.ToString()));
                    retVal.Add(new BasicDataSourceVm("Badge Deactivation Audit", hr.BadgeDeactivate.ToString()));
                }

                return retVal;
            }
        }
    }
}
