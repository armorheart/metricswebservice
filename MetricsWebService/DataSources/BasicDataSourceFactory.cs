﻿using System;
using System.Collections.Generic;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.DataSources.Repo;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class BasicDataSourceFactory : IDataSourceFactory
    {
        private readonly string _ConnStr;
        private readonly string _ConnectionName;
        private readonly ITemplatesService _TemplatesService;
        private readonly IHistoryRepo _HistoryRepo;
        private readonly IMetricsRepo<RealtimeStatConfig> _MetricsRepo;
        private readonly IBadgeCountRepo _BadgeCountRepo;

        public BasicDataSourceFactory(string connStr, 
            string connectionName, ITemplatesService templatesService,
            IHistoryRepo historyRepo, IMetricsRepo<RealtimeStatConfig> metricsRepo,
            IBadgeCountRepo badgeCountRepo)
        {
            _ConnStr = connStr;
            _ConnectionName = connectionName;
            _TemplatesService = templatesService;
            _HistoryRepo = historyRepo;
            _MetricsRepo = metricsRepo;
            _BadgeCountRepo = badgeCountRepo;
        }

        public List<string> GetAllDataSources()
        {
            return new List<string>()
            {
                "HrImport",
                "LiveEvents",
                "Replication",
                "TopAlarms",
                "Metrics",
                "AccessLevel",
                "BadgeCounts",
                "BadgePopulation",
                "Spatial",
                "HardwareCurrentCount",
                "EventCurrentCount",
                "WorkforceBadge"
                //"BadgeTypeCount",
                //"BadgeLocationCount",
                //"BadgeReasonCount"
            };
        }

        public IDataSource CreateSource(string name)
        {
            switch (name.ToLower())
            {
                case "hrimport":
                    return new HrImportDataSource(_ConnectionName, _TemplatesService);
                case "liveevents":
                    return new LiveEventDataSource(_TemplatesService, _MetricsRepo);
                case "replication":
                    return new ReplicationDataSource(_ConnectionName, _TemplatesService);
                case "topalarms":
                    return new TopAlarmsDataSource(_ConnectionName, _TemplatesService);
                case "metrics":
                    return new MetricsDataSource(_TemplatesService, _HistoryRepo);
                case "accesslevel":
                    return new AccessLevelDataSource(_ConnectionName);
                case "badgecounts":
                    return new BadgeCountDataSource(_BadgeCountRepo);
                case "badgepopulation":
                    return new BadgePopulationDataSource(_ConnStr);
                case "spatial":
                    return new SpatialDataSource(_ConnStr);
                case "hardwarecurrentcount":
                    return new HardwareCurrentCountDataSource(_ConnectionName);
                case "eventcurrentcount":
                    return new EventCurrentCountDataSource(_ConnectionName);
                case "workforcebadge":
                    return new WorkforceBadgeDataSource(_ConnectionName);
            }

            throw new Exception(string.Format("DataSource not Found: {0}", name));
        }
    }
}
