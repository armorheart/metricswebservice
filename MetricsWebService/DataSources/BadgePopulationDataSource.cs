﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Models;

namespace MetricsWebService.DataSources
{
    public class BadgePopulationDataSource : IDataSource
    {
        private const string EmpWithContractSql =
            @"SELECT COUNT (*) AS 'Active Employees with Active Contractor Badges ' 
            FROM BADGE 
            WHERE EMPID IN (SELECT ID FROM UDFEMP WHERE EMPTYPE = 10995 AND EMPSTATUS = 11001) 
            AND BADGE.TYPE IN (350, 377) AND BADGE.STATUS = 1";

        private const string ContractWithEmpSql =
            @"SELECT COUNT (*) AS 'Active Contractors with Active Employee Badges' 
            FROM BADGE 
            WHERE EMPID IN (SELECT ID FROM UDFEMP WHERE EMPTYPE = 10994 AND EMPSTATUS = 11001) 
            AND BADGE.TYPE IN (349, 376) AND BADGE.STATUS = 1";

        private const string NoPhotoSql =
            @"SELECT COUNT (*) AS 'Cardholders with Active Employee/Contractor Badges and No Photo' 
            FROM BADGE 
            WHERE EMPID NOT IN (SELECT EMPID FROM MMOBJS WHERE OBJECT = 1 AND TYPE = 0) 
            AND BADGE.TYPE IN (349, 350, 376, 377) AND BADGE.STATUS = 1 ";

        private const string BadWorkingRangeSql =
            @"SELECT COUNT (*) AS 'Cardholders with Active Employee/Contractor Badges outside of Working Range' 
            FROM BADGE 
            WHERE (ID >= 1999999 OR ID <= 1000000)AND BADGE.TYPE IN (349, 350, 376, 377) AND BADGE.STATUS = 1 ";

        private const string MultipleBadgeSql =
            @"SELECT COUNT (MULTIPLE_BADGES.TOTAL) AS 'Cardholders with Multiple Active Employee/Contractor Badges' 
            FROM (SELECT COUNT (BADGEKEY) AS TOTAL 
            FROM BADGE WHERE BADGE.STATUS = 1 AND BADGE.TYPE NOT IN (SELECT ID FROM BADGETYP WHERE NAME LIKE '%Temp%')
            GROUP BY EMPID HAVING COUNT (BADGEKEY) > 1) AS MULTIPLE_BADGES";

        private const string BadgeIdEqEmpIdSql =
            @"SELECT COUNT (*) AS 'Active Employees/Contractors with Badge IDs Matching Employee IDs' 
            FROM BADGE INNER JOIN UDFEMP ON (BADGE.EMPID = UDFEMP.ID AND UDFEMP.EMPTYPE IN (10995, 10994)) 
            WHERE BADGE.ID = CAST (UDFEMP.CISCO_EMPNUM AS INT) AND UDFEMP.EMPSTATUS = 11001 AND CISCO_EMPNUM != 'SYDPAC' ";

        private List<Tuple<string, string>> _Queries = new List<Tuple<string, string>>()
        {
            new Tuple<string, string>("Active Employees with Active Contractor Badges", EmpWithContractSql),
            new Tuple<string, string>("Active Contractors with Active Employee Badges", ContractWithEmpSql),
            new Tuple<string, string>("Cardholders with Active Employee/Contractor Badges and No Photo", NoPhotoSql),
            new Tuple<string, string>("Cardholders with Active Employee/Contractor Badges outside of Working Range",
                BadWorkingRangeSql),
            new Tuple<string, string>("Cardholders with Multiple Active Employee/Contractor Badges", MultipleBadgeSql),
            new Tuple<string, string>("Active Employees/Contractors with Badge IDs Matching Employee IDs",
                BadgeIdEqEmpIdSql)
        };
        

        private readonly string _ConnStr;

        public string Name { get; private set; }

        public BadgePopulationDataSource(string connStr)
        {
            _ConnStr = connStr;
            Name = "BadgePopulation";
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var retVal = new List<object>();
            for (var x = 0; x < _Queries.Count; x++)
            {
                var result = await RunQuery(_Queries[x].Item1, _Queries[x].Item2);
                retVal.Add(result);
            }

            return retVal;
        }

        private async Task<BasicDataSourceVm> RunQuery(string name, string sql)
        {
            try
            {
                using (var conn = new SqlConnection(_ConnStr))
                {
                    await conn.OpenAsync();

                    using (var cmd1 = new SqlCommand(sql, conn))
                    {
                        var value = await cmd1.ExecuteScalarAsync();
                        return new BasicDataSourceVm(name, value.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new BasicDataSourceVm();
            }
        }
    }
}