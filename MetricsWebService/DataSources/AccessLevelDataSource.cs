﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MetricsDash.Models.Details;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using Phase4Lib.Web.DataAccess;

namespace MetricsWebService.DataSources
{
    public class AccessLevelDataSource : IDataSource
    {
        private readonly string _ConnectionName;
        public string Name { get; private set; }

        public AccessLevelDataSource(string connectionName)
        {
            _ConnectionName = connectionName;
            Name = "AccessLevel";
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var commonRepo = new CommonRepo(_ConnectionName);
            var cachedData = await commonRepo.GetCachedDataAsync("Metrics", "AccessLevel", options.Connector);

            if (cachedData == null)
            {
                return new List<object>();
            }

            var dataObj =
                await
                    Task<List<AccessLevel>>.Factory.StartNew(
                        () => Newtonsoft.Json.JsonConvert.DeserializeObject<List<AccessLevel>>(cachedData.Data));

            var retVal = new List<object>();
            retVal.AddRange(dataObj);
            return retVal;
        }
    }
}