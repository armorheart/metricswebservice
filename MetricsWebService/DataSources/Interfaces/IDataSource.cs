﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MetricsDash.Models.Widgets;

namespace MetricsWebService.DataSources.Interfaces
{
    public interface IDataSource
    {
        string Name { get; }

        Task<List<object>> GetDataAsync(WidgetOptions options);

    }
}
