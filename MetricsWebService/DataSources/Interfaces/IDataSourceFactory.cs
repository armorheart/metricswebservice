﻿using System.Collections.Generic;

namespace MetricsWebService.DataSources.Interfaces
{
    public interface IDataSourceFactory
    {
        List<string> GetAllDataSources();
        IDataSource CreateSource(string name);
    }
}
