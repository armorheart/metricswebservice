﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MetricsDash.Cached;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Models;
using Newtonsoft.Json;
using Phase4Lib.Web.DataAccess;

namespace MetricsWebService.DataSources
{
    public class EventCurrentCountDataSource : CurrentCountDataSourceBase, IDataSource
    {
        public string Name { get; }

        public EventCurrentCountDataSource(string connName) : base(connName)
        {
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            try
            {
                return await GetCurrentCounts("EventCurrentCount");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }
    }
}