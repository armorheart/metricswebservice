﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Math;
using MetricsDash.Models;
using MetricsDash.Models.Widgets;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.DataSources.Interfaces;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class LiveEventDataSource : IDataSource
    {
        private readonly ITemplatesService _TemplatesService;
        private readonly IMetricsRepo<RealtimeStatConfig> _Repo;
        public string Name { get; private set; }

        public LiveEventDataSource(ITemplatesService templatesService, IMetricsRepo<RealtimeStatConfig> repo )
        {
            _TemplatesService = templatesService;
            _Repo = repo;
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var live = _Repo.GetMetrics();

            var vmList = new List<LiveEventDsModel>();
            //Get metrics that are out of Tolerance
            foreach (var stat in live)
            {
                //Ignore stats that haven't had a max value set
                if (stat.RealtimeMaxValue == null || stat.RealtimeMaxValue == 0)
                {
                    continue;
                }

                foreach (var val in stat.Values)
                {
                    var vm = new LiveEventDsModel();
                    vm.Name = stat.DisplayName;
                    vm.Connector = val.Server;
                    vm.Value = val.Value ?? 0;
                    //value / max * 100
                    vm.Percent = Formulas.Percent(val.Value ?? 0, stat.RealtimeMaxValue ?? 1);
                    //add it to the list
                    vmList.Add(vm);
                }
            }

            var retVal = new List<object>();
            retVal.AddRange(vmList.Where(p => p.Percent >= 100).OrderByDescending(p => p.Percent));
            return retVal;
        }
    }
}