﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MetricsDash.Cached;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using Newtonsoft.Json;
using Phase4Lib.Web.DataAccess;

namespace MetricsWebService.DataSources
{
    public class WorkforceBadgeDataSource : IDataSource
    {
        private readonly string _ConnectionName;
        public string Name { get; }

        public WorkforceBadgeDataSource(string connectionName)
        {
            _ConnectionName = connectionName;
            Name = "WorkforceBadge";
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var commonRepo = new CommonRepo(_ConnectionName);
            var counts = await commonRepo.GetCachedDataAsync("Metrics", "WorkforceBadge");


            var raw = JsonConvert.DeserializeObject<List<BasicCqModel>>(counts.First()?.Data);

            return raw.Cast<object>().ToList();
        }
    }
}