﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MetricsDash.Cached;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Models;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;
using Phase4Lib.Web.Mvc.Interfaces;

namespace MetricsWebService.DataSources
{
    public class ReplicationDataSource : IDataSource
    {
        private readonly string _ConnectionName;
        private readonly ITemplatesService _TemplatesService;

        public string Name { get; private set; }

        public ReplicationDataSource(string connectionName, ITemplatesService templatesService)
        {
            _ConnectionName = connectionName;
            _TemplatesService = templatesService;
        }

        public async Task<List<object>> GetDataAsync(WidgetOptions options)
        {
            var commonRepo = new CommonRepo(_ConnectionName);
            var regions = new List<CachedData>();
            if (options.Connector == 0)
            {
                regions = await commonRepo.GetCachedDataAsync("Metrics", "Replication");
            }
            else
            {
                var cachedData = await commonRepo.GetCachedDataAsync("Metrics", "Replication", options.Connector);
                if (cachedData != null)
                {
                    regions.Add(cachedData);    
                }
                
            }


            if (!regions.Any())
            {
                return new List<object>();
            }

            var retVal = new List<object>();

            foreach (var cachedData in regions)
            {
                var dataObj =
                    await
                        Task<List<ReplicationCqModel>>.Factory.StartNew(
                            () =>
                                Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReplicationCqModel>>(cachedData.Data));


                retVal.Add(new CachedDataVm()
                {
                    ConnectorId = cachedData.ConnectorId,
                    Data = dataObj
                });
            }

            return retVal;
        }
    }
}