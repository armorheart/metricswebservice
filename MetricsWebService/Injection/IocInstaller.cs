﻿using System.Configuration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using MetricsDash.Models;
using MetricsDash.Repo;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.Services;
using Phase4Lib.Web.Mvc.Interfaces;
using Microsoft.Owin.Security.OAuth;
using MetricsWebService.Providers;

namespace MetricsWebService.Injection
{
    public class IocInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var connStr = ConfigurationManager.ConnectionStrings["Sec"].ConnectionString;
            var connName = "Sec";

            container.Register(
                Component.For<IMetricsRepo<StatConfig>>()
                .ImplementedBy<MetricsRepo>()
               .Named("MetricsRepo")
                .LifestyleTransient()
                .DependsOn(Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName)),
                  Component.For<IMetricsRepo<RealtimeStatConfig>>()
                .ImplementedBy<RealtimeMetricsRepo>()
                .Named("RealtimeMetricsRepo")
                .LifestyleTransient()
                .DependsOn(Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName)),
                Classes.FromAssemblyNamed("MetricsDash")
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName))
                    )
                );
            container.Register(
                Classes.FromAssemblyNamed("Phase4Lib")
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connectionName", connName))
                    )
                );
            container.Register(
                Component.For<ITemplatesService>()
                .ImplementedBy<RazorTemplateService>()
                .LifestyleTransient(),
                Component.For< IOAuthAuthorizationServerProvider >()
                .ImplementedBy< Dd2AuthorizationServerProvider >()
                .LifestyleTransient(),
                Classes.FromThisAssembly()
                    .Pick()
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient()
                    .Configure(i => i.DependsOn(
                        Dependency.OnValue("connStr", connStr),
                        Dependency.OnValue("connectionName", connName))
                    )
                );
        }
    }
}