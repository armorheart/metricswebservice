﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
//using System.Web.Http.Cors;
using MetricsDash.UserManagement;
using MetricsWebService.Filters;
using Phase4Lib.Web.Authentication;
using Phase4Lib.Web.Ldap;
using Phase4Lib.Web.Models;

namespace MetricsWebService.Controllers
{
    
    
    [RoutePrefix("api/user")]
    public class UserController : BaseApiController
    {
        private readonly IUserRepo _UserRepo;
        private readonly IRoleManager _RoleManager;

        public UserController(IUserRepo userRepo, IRoleManager roleManager)
        {
            _UserRepo = userRepo;
            _RoleManager = roleManager;
        }

       

        [WebApiAuthorize(MappedRoles = "admin")]
        [HttpGet]
        [Route()]
        public List<MetricUser> Get()
        {
            var users = _UserRepo.GetUsers();

            return users.ToList();
        }

        //Everyone can get their own details
        [WebApiAuthorize()]
        [HttpGet]
        [Route("{userId}")]
        public MetricUser Get(string userId)
        {
            if (!AllowLookup(userId))
            {
                throw new NotAuthorizedException("Users can only lookup themselves");
            }

            //Make an exception for the default admin
            if (IsDefaultAdmin(userId))
            {
                var admin = GetDefaultAdmin();
                if (admin == null)
                {
                    throw new ApiException(HttpStatusCode.NotFound, "No Such User");
                }
                return admin;
            }

            var user = _UserRepo.GetUser(userId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, "No Such User");
            }

            //Add the PermissionList
            var roles = _RoleManager.GetRolesForUser(user);
            user.PermissionList = roles.ToList();

            return user;
        }

        /// <summary>
        /// Add new User
        /// </summary>
        /// <param name="user"></param>
        [WebApiAuthorize(MappedRoles = "admin")]
        [HttpPost]
        [Route()]
        public MetricUser Post([FromBody] MetricUser user)
        {
            return _UserRepo.UpdateUser(user);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        [WebApiAuthorize(MappedRoles = "admin")]
        [HttpPut]
        [Route()]
        public MetricUser Put([FromBody] MetricUser user)
        {
            return _UserRepo.UpdateUser(user);
        }

        [WebApiAuthorize(MappedRoles = "admin")]
        [HttpDelete]
        [Route()]
        public void Delete(int id)
        {
            _UserRepo.DeleteUser(id);
        }

        

        private MetricUser GetDefaultAdmin()
        {
            var ldapConfig = DD2LdapConfigSection.GetConfig();
            if (ldapConfig == null)
            {
                return null;
            }

            var user = new MetricUser()
            {
                AccountStatus = UserAccountStatus.Enabled,
                Email = "",
                FirstName = "Admin",
                LastName = "Admin",
                LdapAccount = ldapConfig.DD2LdapConfig.ServiceUser,
                Permissions = 32,
                PermissionList = new List<Role>() {new Role("Admin", 32)},
                UserId = ldapConfig.DD2LdapConfig.ServiceUser,
                SsbrAdmin = true
            };

            return user;
        }
    }
}
