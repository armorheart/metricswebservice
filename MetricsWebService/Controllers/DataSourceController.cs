﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using MetricsDash.Models.Widgets;
using MetricsWebService.DataSources.Interfaces;
using MetricsWebService.Filters;

namespace MetricsWebService.Controllers
{
    /// <summary>
    /// Service to get various dataSources for widgets and graphs
    /// </summary>
    public class DataSourceController : BaseApiController
    {
        private readonly IDataSourceFactory _Factory;

        public DataSourceController(IDataSourceFactory factory)
        {
            _Factory = factory;
        }

        /// <summary>
        /// Get a list of available DataSources for widgets
        /// current: "HrImport", "LiveEvents", "Replication", "TopAlarms", "Metrics", 
        /// "AccessLevel, BadgeCounts, BadgePopulation"
        /// </summary>
        /// <returns></returns>
        public List<string> Get()
        {
            return _Factory.GetAllDataSources();
        }

        /// <summary>
        /// Get Data from the specific source
        /// Replication, TopAlarms, and AccessLevel require a connectorId
        /// TopAlarms also requires a TopX
        /// BadgeCount requires a StartDate and EndDate
        /// </summary>
        /// <param name="id">id of the datasource from Get() call</param>
        /// <param name="options">Options to specify a database or limit results</param>
        /// <returns>varies by dataSource</returns>
        public async Task<List<object>> Get(string id, [FromUri]WidgetOptions options)
        {
            var dataSource = _Factory.CreateSource(id);
            return await dataSource.GetDataAsync(options);
        }
    }
}
