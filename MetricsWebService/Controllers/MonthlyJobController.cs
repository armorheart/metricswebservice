﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsWebService.Controllers
{
    [RoutePrefix("MonthlyJob/api")]
    public class MonthlyJobController : BaseApiController
    {
        private readonly IHistoryRepo _Repo;

        public MonthlyJobController(IHistoryRepo repo)
        {
            _Repo = repo;
        }

        [Route("rerun/{id}/{server:int}/{year:int}/{month:int}")]
        public IHttpActionResult ReRun(string id, int server, int year, int month)
        {
            var metricInstance = _Repo.GetSingleHistory(id, year, month, Interval.Monthly);

            //Get existing value for the given data

            //update that value

            return Ok();
        }
    }
}
