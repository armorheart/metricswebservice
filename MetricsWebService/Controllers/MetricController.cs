﻿using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsWebService.Controllers
{
    public class MetricController : MetricControllerBase<StatConfig>
    {
        public MetricController(IMetricsRepo<StatConfig> metricsRepo)
        {
            _MetricsRepo = metricsRepo;
        }
    }
}
