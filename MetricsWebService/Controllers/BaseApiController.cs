﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Phase4Lib.Web.Ldap;

namespace MetricsWebService.Controllers
{
    public class BaseApiController : ApiController
    {
        protected bool IsDefaultAdmin(string userName)
        {
            //See if the user is the default admin
            var ldapConfig = DD2LdapConfigSection.GetConfig();
            if (ldapConfig == null)
            {
                return false;
            }

            return userName.ToLower().Equals(ldapConfig.DD2LdapConfig.ServiceUser);
        }

        /// <summary>
        /// By default, users can only lookup their own data
        /// So the id must match the authenticated user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool AllowLookup(string id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            //bypass if is in admin role
            if (claims.Any(
                c =>
                    c.Type == ClaimTypes.Role &&
                    c.Value.Equals("admin", StringComparison.InvariantCultureIgnoreCase)))
            {
                return true;
            }

            var u = claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);

            if (u != null)
            {
                var name = u.Value.Split(new char[] { '\\' }).Last();
                if (name == id)
                {
                    return true;
                }
            }

            var user = claims.FirstOrDefault(c => c.Type == "sub");
            if (user != null && user.Value.ToLower() == id)
            {
                return true;
            }

            return false;
        }
    }
}