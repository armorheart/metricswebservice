﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.Filters;

namespace MetricsWebService.Controllers
{
    [RoutePrefix("api/history")]
    public class HistoryController : BaseApiController
    {
        private IHistoryRepo _Repo;

        public HistoryController(IHistoryRepo repo)
        {
            _Repo = repo;
        }


            /// <summary>
        /// Gets History for a Individual Metric
        /// </summary>
        /// <param name="id">Column Name</param>
        [HttpGet]
        [Route("{id}")]
        public SingleHistoryVm Get(string id)
        {
            var pastYear = new DateTime(1900, 1, 1);
            var single = _Repo.GetSingleHistory(id, pastYear, DateTime.UtcNow, Interval.Monthly);
            return single;
        }
        
        
        /// <summary>
        /// Gets History for a given year
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("prev/{year}")]
        public IEnumerable<MetricVm> Prev(int year)
        {
            var latest = _Repo.GetMetricsTotals(year);
            return latest;
        }

        /// <summary>
        /// Gets History for a given year and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("prev/{year}/{month}")]
        public IEnumerable<MetricVm> Prev(int year, int month)
        {
            var latest = _Repo.GetMetricsTotals(year, month);
            return latest;
        }

        /// <summary>
        /// Gets History for a Individual Metric 
        /// </summary>
        /// <param name="id">Column Name</param>
        /// <param name="year">year requested</param>
        /// <param name="month">month requested</param>
        [HttpGet]
        [Route("single/{id}/{month}/{year}")]
        public SingleHistoryVm Single(string id, int month, int year)
        {
            var single = _Repo.GetSingleHistory(id, year, month, Interval.Monthly);
            return single;
        }

        [HttpGet]
        [Route("single/{id}/{month}")]
        public SingleHistoryVm Single(string id, int month)
        {
            var single = _Repo.GetSingleHistory(id, month);
            return single;
        }

       
        /// <summary>
        /// Gets Metrics for the latest month
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("latest")]
        public IList<MetricVm> Latest()
        {
            var latest = _Repo.GetLastMetricsTotals().ToList();
            return latest;
        }

        /// <summary>
        /// Gets the latest metric values for the selected metric Ids
        /// </summary>
        /// <param name="ids">comma separated list of metric column names</param>
        /// <returns></returns>
        [HttpGet]
        [Route("latest/{ids}")]
        public IList<MetricVm> Latest(string ids)
        {
            var latest = _Repo.GetLastMetricTotals(ids.Split(',')).ToList();
            return latest;
        }

        

    }
}
