﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.Filters;

namespace MetricsWebService.Controllers
{
    public class LookupController : BaseApiController
    {
        private readonly IHistoryRepo _Repo;

        public LookupController(IHistoryRepo repo)
        {
            _Repo = repo;
        }

        /// <summary>
        /// Gets the types of lookup options
        /// </summary>
        /// <returns></returns>
        [Route("api/Lookup")]
        public List<string> Get()
        {
            return new List<string>
            {
                "Category",
                "SubCategory",
                "ConnectorType",
                "DataType",
                "AccountStatus",
                "StatStatus",
                "Permissions"
            };
        }

        /// <summary>
        /// Gets the values for a type of lookup
        /// </summary>
        /// <param name="id">currently "Category", SubCategory", "ConnectorType", "DataType"</param>
        /// <returns></returns>
        [Route("api/Lookup/{id}")]
        public List<LookupItem> Get(string id)
        {
            return _Repo.GetLookupValues(id);
        }
    }
}
