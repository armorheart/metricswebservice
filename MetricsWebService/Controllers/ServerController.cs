﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MetricsWebService.Models;
using Phase4Lib.Web.Authentication;
using Phase4Lib.Web.DataAccess;
using Phase4Lib.Web.Models;

namespace MetricsWebService.Controllers
{
    /// <summary>
    /// CRUD operations for Datasource Connectors
    /// </summary>
    [WebApiAuthorize(MappedRoles = "admin")]
    public class ServerController : BaseApiController
    {
        private readonly ICommonRepo _Repo;

        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ServerController(ICommonRepo repo)
        {
            _Repo = repo;
        }

        /// <summary>
        /// Returns the list of Servers that are sources of metric data
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/server")]
        public List<ConnectorConfig> Get()
        {
            _Log.Debug("Getting servers");

            var servers = _Repo.GetRegions();
            return servers.ToList();

        }

        /// <summary>
        /// Adds a new server
        /// </summary>
        /// <param name="server"></param>
        [HttpPost]
        [Route("api/Server")]
        public void Add([FromBody] ConnectorConfig server)
        {
            _Repo.CreateRegion(server);
        }

        /// <summary>
        /// Update an existing server
        /// </summary>
        /// <param name="server"></param>
        [HttpPut]
        [Route("api/Server")]
        public void Update([FromBody] ConnectorConfig server)
        {
            _Repo.UpdateRegion(server);
        }

        /// <summary>
        /// Delete a server
        /// </summary>
        /// <param name="server"></param>
        [HttpDelete]
        [Route("api/Server/{id}")]
        public void Delete(int id)
        {
            try
            {
                _Log.DebugFormat("Trying to delete server: {0}", id);
                _Repo.DeleteRegion(id);
            }
            catch (Exception e)
            {
                _Log.Error("Deleting Server", e);
            }
        }
    }
}
