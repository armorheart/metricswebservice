﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.Filters;
using Phase4Lib.Web.Authentication;

namespace MetricsWebService.Controllers
{
    [RoutePrefix("api/Live")]
    public class LiveController : BaseApiController
    {
        private readonly IHistoryRepo _Repo;
        private readonly IMetricsRepo<StatConfig> _MetricsRepo;
        private readonly IMetricsRepo<RealtimeStatConfig> _RealtimeRepo;

        public LiveController(IHistoryRepo repo, 
            IMetricsRepo<StatConfig> metricsRepo, 
            IMetricsRepo<RealtimeStatConfig> realtimeRepo )
        {
            _Repo = repo;
            _MetricsRepo = metricsRepo;
            _RealtimeRepo = realtimeRepo;
        }

        /// <summary>
        /// Gets the live stats for a given metric
        /// </summary>
        /// <param name="id">Column Name of the metric</param>
        /// <returns></returns>
        [HttpGet]
        [Route("Metric/{id}")]
        public IEnumerable<MetricShort> Metric(string id)
        {
            var stat = _MetricsRepo.GetMetric(id);
            var liveStats = new List<MetricShort>();

            foreach (var connector in stat.ServerSel.Split(','))
            {
                liveStats.Add(_MetricsRepo.RunMetricQuery(stat, connector, DateTime.UtcNow));
            }

            return liveStats.Where(m => !string.IsNullOrEmpty(m.ColumnName));
        }

        [HttpGet]
        [Route("Realtime/{id}")]
        public IEnumerable<MetricShort> Realtime(string id)
        {
            var stat = _RealtimeRepo.GetMetric(id);
            var liveStats = new List<MetricShort>();

            if (stat == null)
                return liveStats;

            foreach (var connector in stat.ServerSel.Split(','))
            {
                liveStats.Add(_RealtimeRepo.RunMetricQuery(stat, connector, DateTime.UtcNow));
            }

            return liveStats.Where(m => !string.IsNullOrEmpty(m.ColumnName));
        }


        
    }
}
