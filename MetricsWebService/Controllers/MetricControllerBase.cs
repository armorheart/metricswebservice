﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;
using MetricsWebService.Filters;

namespace MetricsWebService.Controllers
{
    public class MetricControllerBase<T> : BaseApiController where T : StatConfigBase
    {
        protected IMetricsRepo<T> _MetricsRepo;

        public List<T> Get()
        {
            var m = _MetricsRepo.GetMetrics();
            return m;
        }

        public T Get(string id)
        {
            var m = _MetricsRepo.GetMetric(id);
            return m;
        }

        /// <summary>
        /// Add a new Metric
        /// </summary>
        /// <param name="metric"></param>
        [HttpPost]
        public void Post(T metric)
        {
            //Validate column name
            if (!metric.ValidateColumnName())
            {
                throw new ApiException(HttpStatusCode.NotAcceptable, string.Format(
                    "Invalid Column Name: {0}",
                    metric.ColumnName));

            }

            //check to see if their already is a metric with the same column name
            var existing = _MetricsRepo.GetMetric(metric.ColumnName);
            if (existing != null)
            {
                throw new ApiException(HttpStatusCode.NotAcceptable, string.Format(
                    "Metric with this column name already exists"));

            }

            _MetricsRepo.UpdateMetric(metric, true);
        }

        /// <summary>
        /// Update and Existing Metric
        /// </summary>
        /// <param name="metric"></param>
        [HttpPut]
        public void Put(T metric)
        {
            //Get the existing metric
            _MetricsRepo.UpdateMetric(metric, false);
        }

        /// <summary>
        /// Deletes an Existing Metric
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        public void Delete(string id)
        {
            _MetricsRepo.DeleteMetric(id);
        }
    }
}
