﻿using MetricsDash.Models;
using MetricsDash.Repo.Interfaces;

namespace MetricsWebService.Controllers
{
    public class RealtimeController : MetricControllerBase<RealtimeStatConfig>
    {
        public RealtimeController(IMetricsRepo<RealtimeStatConfig> metricsRepo)
        {
            _MetricsRepo = metricsRepo;
        }
    }
}
