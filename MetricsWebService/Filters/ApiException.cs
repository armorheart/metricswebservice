﻿using System;
using System.Net;

namespace MetricsWebService.Filters
{
    public class ApiException : Exception
    {
        public string Msg { get; protected set; }
        public HttpStatusCode StatusCode { get; protected set; }

        public ApiException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        public ApiException(HttpStatusCode statusCode, string message)
            : this(statusCode)
        {
            Msg = message;
        }
    }
}