﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace MetricsWebService.Results
{
    public class HtmlActionResult : System.Web.Http.IHttpActionResult
    {
        private readonly string _ViewName;
        private readonly dynamic _Model;

        private const string ViewFolder = "Templates";

        public HtmlActionResult(string viewName, dynamic model)
        {
            _ViewName = LoadView(viewName);
            _Model = model;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var parsedView = RazorEngine.Razor.Parse(_ViewName, _Model);
            response.Content = new StringContent(parsedView);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return Task.FromResult(response);
        }

        private string LoadView(string name)
        {
            var view = File.ReadAllText(Path.Combine(GetCurrentDirectory(), ViewFolder, name + ".cshtml"));
            return view;
        }

        private string GetCurrentDirectory()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }
    }
}