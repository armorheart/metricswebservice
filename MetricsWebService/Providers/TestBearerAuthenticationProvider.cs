﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Security.OAuth;

namespace MetricsWebService.Providers
{
    public class TestBearerAuthenticationProvider : OAuthBearerAuthenticationProvider
    {
        public override async Task RequestToken(OAuthRequestTokenContext context)
        {
            await base.RequestToken(context);
        }
        public override async Task ValidateIdentity(OAuthValidateIdentityContext context)
        {
            await base.ValidateIdentity(context);
        }
    }
}