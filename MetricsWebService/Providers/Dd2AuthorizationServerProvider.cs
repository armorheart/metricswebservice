﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using log4net;
using MetricsDash.UserManagement;
using Microsoft.Owin.Security.OAuth;
using Phase4Lib.Audit;
using Phase4Lib.Web.Ldap;
using Phase4Lib.Web.Models;

namespace MetricsWebService.Providers
{
    public class Dd2AuthorizationServerProvider : OAuthAuthorizationServerProvider, IDisposable
    {
        private readonly IUserRepo _UserRepo;
        private readonly ILdapConnection _LdapConnection;
        private readonly IAuditRepo _AuditRepo;
        private readonly IRoleManager _RoleManager;
        private static readonly log4net.ILog _Log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Dd2AuthorizationServerProvider(IUserRepo userRepo, 
            ILdapConnection ldapConnection, 
            IAuditRepo auditRepo, 
            IRoleManager roleManager
            )
        {
            _UserRepo = userRepo;
            _LdapConnection = ldapConnection;
            _AuditRepo = auditRepo;
            _RoleManager = roleManager;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async  Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            _Log.Debug($"Starting Auth for {context.UserName}");
            //See if the user is the default admin
            var ldapConfig = DD2LdapConfigSection.GetConfig();
            if (ldapConfig == null)
            {
                context.SetError("invalid_config");
                _Log.Error($"Invalid Login Config {context.UserName}");
                return;
            }

            _Log.Debug($"Got Config for {context.UserName}");

            if (!context.OwinContext.Response.Headers.ContainsKey("Access-Control-Allow-Origin"))
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            }

            if (!context.OwinContext.Response.Headers.ContainsKey("Access-Control-Allow-Headers"))
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "Cache-Control", "Pragma", "Origin", "Authorization", "Content-Type", "X-Requested-With" });
            }

            if (!context.OwinContext.Response.Headers.ContainsKey("Access-Control-Allow-Methods"))
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET", "PUT", "POST", "OPTIONS", "DELETE" });
            }

            try
            {
                
                var isDefaultAdmin = false;
                
                //See if the user is a registered User
                var user = _UserRepo.GetUser(context.UserName);

                //Check for default admin status
                if (context.UserName.ToLower().Equals(ldapConfig.DD2LdapConfig.ServiceUser))
                {
                    _LdapConnection.TestCredentials(context.UserName, context.Password, true);
                    isDefaultAdmin = true;
                }
                else
                {
                    if (user == null)
                    {
                        _Log.Error($"Couldn't find user {context.UserName}");
                        context.SetError("invalid_grant", "The user name or password is incorrect.");
                        return;
                    }

                    _Log.Debug($"Got user record for {context.UserName}");

                    if (user.AccountStatus == UserAccountStatus.Disabled)
                    {
                        _Log.Error($"User is disabled {context.UserName}");
                        context.SetError("invalid_grant", "The user name or password is incorrect.");
                        return;
                    }

                    try
                    {
                        var success = _LdapConnection.TestCredentials(context.UserName, context.Password);

                        _Log.Debug($"Tested Creds for {context.UserName}");

                        if (success)
                        {
                            _AuditRepo.Add(user.Id, "Metrics", "Login");

                            _Log.Debug($"Added Audit for {context.UserName}");
                            //set the last login field
                            user.LastLogin = DateTime.UtcNow;
                            _UserRepo.UpdateUser(user);

                            _Log.Debug($"Updated user record for {context.UserName}");
                        }

                        if (!success)
                        {
                            _Log.Error($"LDAP returned failure for {context.UserName}");
                            context.SetError("invalid_grant", "The user name or password is incorrect.");
                            return;
                        }

                    }
                    catch (Exception e)
                    {
                        _Log.Error($"Ldap Threw Error for {context.UserName}, {e.GetBaseException().Message}", e);
                        context.SetError("invalid_grant", "The user name or password is incorrect.");
                        throw;
                    }
                }

                _Log.Debug($"Adding Claims for {context.UserName}");

                //Set Claims
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim("sub", context.UserName));

                if (isDefaultAdmin)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                }
                else
                {
                    var roles = _RoleManager.GetRolesForUser(user);

                    _Log.Debug($"Got Roles for {context.UserName}");

                    foreach (var role in roles)
                    {
                        identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
                    }

                }

                context.Validated(identity);
            }
            catch (Exception e)
            {
                _Log.Error($"Other Login Failure for {context.UserName}, {e.GetBaseException().Message}", e);
                context.SetError("invalid_grant", e.Message);
            }

            

            

        }

        public void Dispose()
        {
            _LdapConnection.Dispose();
        }
    }
}