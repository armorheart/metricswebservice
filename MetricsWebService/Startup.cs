﻿using System;
using System.Configuration;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.Windsor;
using MetricsWebService;
using MetricsWebService.Injection;
using MetricsWebService.Providers;
using MetricsWebServiceV2;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Phase4Lib.Logging;

[assembly: OwinStartup(typeof(MetricsRestService.Startup))]
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace MetricsRestService
{
    public class Startup
    {
        private static ContainerBootstrapper Bootstrapper;

        public void Configuration(IAppBuilder app)
        {
            AreaRegistration.RegisterAllAreas();

            //Setup Castle Windsor Dependency Injection
            Bootstrapper = ContainerBootstrapper.Bootstrap();

            var config = new HttpConfiguration();

            ConfigureOAuth(app, Bootstrapper.Container);

            config.EnableCors(new EnableCorsAttribute("*", "*", "GET, POST, OPTIONS, PUT, DELETE"));

            config.Services.Replace(
                typeof(IHttpControllerActivator),
                new WindsorCompositionRoot(Bootstrapper.Container));

            

            WebApiConfig.Register(config);

            app.UseWebApi(config);

            Log4NetPropertyHelper.SetupLog4Net(ConfigurationManager.AppSettings["appName"]);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            RegisterShutdownCallback(app, Bootstrapper.Container);
        }

        public void ConfigureOAuth(IAppBuilder app, IWindsorContainer container)
        {
            var provider = container.Resolve<IOAuthAuthorizationServerProvider>();
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = provider
            };

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
            {
                Provider = new TestBearerAuthenticationProvider()
            });
        }

        public void RegisterShutdownCallback(IAppBuilder app, IWindsorContainer container)
        {
            var context = new OwinContext(app.Properties);
            var token = context.Get<CancellationToken>("host.OnAppDisposing");

            if (token != CancellationToken.None)
            {
                token.Register(container.Dispose);
            }
        }



    }
}
